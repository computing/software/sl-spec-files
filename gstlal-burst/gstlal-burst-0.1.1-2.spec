%define gstreamername gstreamer1

Name: gstlal-burst
Version: 0.1.1
Release: 2%{?dist}
Summary: GSTLAL Burst
License: GPL
Group: LSC Software/Data Analysis

Requires: gstlal-ugly >= 1.6.0
Requires: gstlal >= 1.5.0
Requires: python >= 2.7
Requires: glue >= 1.59.3
Requires: python2-ligo-segments >= 1.2.0
Requires: gobject-introspection >= 1.30.0
Requires: fftw >= 3
Requires: python-%{gstreamername}
Requires: %{gstreamername} >= 1.14.1
Requires: %{gstreamername}-plugins-base >= 1.14.1
Requires: %{gstreamername}-plugins-good >= 1.14.1
Requires: h5py
Requires: numpy
Requires: scipy
Requires: lal >= 6.19.0
Requires: lalmetaio >= 1.4.0
Requires: lalburst >= 1.5.0
Requires: orc >= 0.4.16
Requires: gsl
BuildRequires: gobject-introspection-devel >= 1.30.0
BuildRequires: gstlal-devel >= 1.5.0
BuildRequires: python-devel >= 2.7
BuildRequires: fftw-devel >= 3
BuildRequires: %{gstreamername}-devel >= 1.14.1
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.14.1
BuildRequires: lal-devel >= 6.19.0
BuildRequires: lal-python >= 6.19.0
BuildRequires: lalburst-devel >= 1.5.0
BuildRequires: lalmetaio-devel >= 1.4.0
BuildRequires: gsl-devel
BuildRequires: graphviz
BuildRequires: orc >= 0.4.16

Conflicts: gstlal-ugly < 0.6.0
Source: gstlal-burst-%{version}.tar.gz
URL: https://www.lsc-group.phys.uwm.edu/daswg/projects/gstlal.html
Packager: Chris Pankow <chris.pankow@gravity.phys.uwm.edu>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package contains the plugins and shared libraries required to run the gstlal burst (generic transient) pipeline.

%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version} 
Requires: gstlal-devel >= 1.5.0
Requires: gstlal-ugly-devel >= 1.6.0
Requires: python-devel >= 2.7 
Requires: fftw-devel >= 3 
Requires: %{gstreamername}-devel >= 1.14.1
Requires: %{gstreamername}-plugins-base-devel >= 1.14.1 
Requires: lal-devel >= 6.19.0
Requires: lalmetaio-devel >= 1.4.0
Requires: lalburst-devel >= 1.5.0

%description devel
This package contains the files needed for building gstlal-burst based plugins
and programs.

%prep
%setup -q -n %{name}-%{version}


%build
%configure
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal
%{_libdir}/gstreamer-1.0/*.so
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_libdir}/gstreamer-1.0/*.a
%{_includedir}/*
