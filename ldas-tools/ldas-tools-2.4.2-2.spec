# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define _docdir %{_datadir}/doc/ldas-tools-%{version}

Summary: Suite of LDAS tools
Name: ldas-tools
Version: 2.4.2
Release: 2
License: ?
URL: http://www.ligo.caltech.edu
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: http://ldas-sw.ligo.caltech.edu/packages/ldas-tools-%{version}.tar.gz
Requires: ldas-tools-al, ldas-tools-framecpp, ldas-tools-framecpp-python, ldas-tools-framecppc, ldas-tools-filters, ldas-tools-genericAPI, ldas-tools-genericAPI-python, ldas-tools-frameAPI, ldas-tools-frameAPI-python, ldas-tools-diskcacheAPI, ldas-tools-diskcacheAPI-python, ldas-tools-utilities
Buildrequires: autoconf, automake, libtool, zlib-devel, openssl-devel, flex, bison, swig, python-devel, doxygen

%description
This is a collection of tools developed as part of the LDAS project.

%package devel
Group: Application/Scientific
Requires: ldas-tools-al-devel, ldas-tools-framecpp-devel, ldas-tools-framecpp-python-devel, ldas-tools-framecppc-devel, ldas-tools-filters-devel, ldas-tools-genericAPI-devel, ldas-tools-frameAPI-devel, ldas-tools-diskcacheAPI-devel, ldas-tools-utilities-devel
Summary: Meta package of LDAS Tools development libraries and tools
%description devel
This provides the development files for the LDAS tool suite

%package doc
Group: Application/Scientific
Requires: ldas-tools-framecpp-doc
Summary: LDAS tools documentation
%description doc
This provides the documentation for the LDAS Tools Suite.

%package minimal-python
Group: Application/Scientific
Summary: LDAS tools python
%description minimal-python
This provides the minimal-python setup

%package al
Obsoletes: ldas-tools-general
Group: Application/Scientific
Requires: zlib openssl
Summary: LDAS tools abstraction toolkit runtime files
%description al
This provides the runtime libraries for the abstaction library.

%package al-devel
Obsoletes: ldas-tools-general-devel
Group: Development/Scientific
Requires: ldas-tools-al
Summary: LDAS tools abstraction toolkit development files
%description al-devel
This provides the develpement files the abstraction library.

%package framecpp
Group: Application/Scientific
Requires: ldas-tools-al
Summary: LDAS tools libframecpp toolkit runtime files
%description framecpp
This provides the runtime libraries for the framecpp library.

%package framecpp-python
Group: Application/Scientific
Requires: ldas-tools-framecpp, ldas-tools-minimal-python
Summary: Python extension for frameCPP
%description framecpp-python
This provides the libraries needed to utilize the frameCPP library
from within Python

%package framecpp-devel
Group: Development/Scientific
Requires: ldas-tools-al-devel, ldas-tools-framecpp
Summary: LDAS tools libframecpp toolkit development files
%description framecpp-devel
This provides the develpement files the framecpp library.

%package framecpp-doc
Group: Development/Scientific
Summary: LDAS tools libframecpp documentation
%description framecpp-doc
This provides the documentation for the framecpp library.

%package framecppc
Obsoletes: ldas-tools-framec
Group: Application/Scientific
Requires: ldas-tools-framecpp
Summary: LDAS tools c wrapping of libframecpp
%description framecppc
This provides the runtime libraries for the framecpp library.

%package framecppc-devel
Obsoletes: ldas-tools-framec-devel
Requires: ldas-tools-framecpp-devel
Group: Development/Scientific
Summary: LDAS tools libframec toolkit development files
%description framecppc-devel
This provides the develpement files the framecpp library.

%package filters
Group: Application/Scientific
Requires: ldas-tools-al
Summary: LDAS tools libfilters toolkit runtime files
%description filters
This provides the runtime libraries for the filters library.

%package filters-devel
Group: Development/Scientific
Requires: ldas-tools-al-devel, ldas-tools-filters
Summary: LDAS tools libfilters toolkit development files
%description filters-devel
This provides the develpement files the filters library.

%package genericAPI
Group: Application/Scientific
Requires: ldas-tools-al
Summary: LDAS tools libgenericAPI toolkit runtime files
%description genericAPI
This provides the runtime libraries for the genericAPI library.

%package genericAPI-python
Group: Application/Scientific
Requires: ldas-tools-genericAPI, ldas-tools-minimal-python
Summary: LDAS tools genericAPI toolkit python files
%description genericAPI-python
This provides the libraries needed to utilize the genericAPI library
from within Python

%package genericAPI-devel
Group: Development/Scientific
Requires: ldas-tools-al-devel, ldas-tools-genericAPI
Summary: LDAS tools libgenericAPI toolkit development files
%description genericAPI-devel
This provides the develpement files the genericAPI library.

%package frameAPI
Group: Application/Scientific
Requires: ldas-tools-filters, ldas-tools-genericAPI
Summary: LDAS tools libframeAPI toolkit runtime files
%description frameAPI
This provides the runtime libraries for the frameAPI library.

%package frameAPI-python
Group: Application/Scientific
Requires: ldas-tools-frameAPI, ldas-tools-minimal-python
Summary: LDAS tools libframeAPI toolkit runtime files
%description frameAPI-python
This provides the libraries needed to utilize the frameAPI library
from within Python

%package frameAPI-devel
Group: Development/Scientific
Requires: ldas-tools-al-devel, ldas-tools-filters-devel, ldas-tools-frameAPI
Summary: LDAS tools libframeAPI toolkit development files
%description frameAPI-devel
This provides the develpement files the frameAPI library.

%package diskcacheAPI
Group: Application/Scientific
Requires: ldas-tools-genericAPI
Summary: LDAS tools libdiskcacheAPI toolkit runtime files
%description diskcacheAPI
This provides the runtime libraries for the diskcacheAPI library.

%package diskcacheAPI-python
Group: Application/Scientific
Requires: ldas-tools-diskcacheAPI, ldas-tools-minimal-python
Summary: Python% extension for the disk cache
%description diskcacheAPI-python
This provides the libraries needed to utilize the disk cache library
from within Python

%package diskcacheAPI-devel
Group: Development/Scientific
Requires: ldas-tools-genericAPI-devel, ldas-tools-diskcacheAPI
Summary: LDAS tools libdiskcacheAPI toolkit development files
%description diskcacheAPI-devel
This provides the develpement files the diskcacheAPI library.

%package utilities
Group: Application/Scientific
Requires: ldas-tools-diskcacheAPI, ldas-tools-frameAPI
Summary: LDAS tools utility programs
%description utilities
This provides utilities that are useful to end users

%prep

%setup -q

%build

%configure --with-optimization=high --disable-tcl --enable-python --docdir=%{_docdir}
make %{?_smp_mflags}
make check

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make install DESTDIR=%{buildroot}
# make lscsoft-install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;
rm -f %{buildroot}%{python_sitearch}/LDAStools/_*.a
rm -f %{buildroot}%{python_sitearch}/LDAStools/libdiskcacheAPI_python.a

%post
ldconfig
%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
#%{_sysconfdir}/ldas-tools-user-env.*

%files doc
%defattr(-,root,root)
%doc %{_docdir}/html

%files minimal-python
%defattr(-,root,root,-)
%{python_sitearch}/LDAStools/__init__.py*

%files al
%defattr(-,root,root,-)
##%{_bindir}
%{_libdir}/libexception*.so.*
%{_libdir}/libldastoolsal*.so.*

%files al-devel
%defattr(-,root,root)
%{_includedir}/ldastoolsal
%{_includedir}/ldas/ldasconfig.hh
%{_libdir}/libexception.a
%{_libdir}/libexception*.so
%{_libdir}/libldastoolsal.a
%{_libdir}/libldastoolsal*.so
%{_libdir}/pkgconfig/ldastoolsal.pc
%{_docdir}/ldastoolsal

%files framecpp
%defattr(-,root,root,-)
%exclude %{_libdir}/libframecppc.so*
%exclude %{_bindir}/framecpp_*.py
%{_bindir}/framecpp_*
%{_libdir}/libframecpp*.so.*

%files framecpp-python
%defattr(-,root,root,-)
%{_bindir}/framecpp_*.py
%{python_sitearch}/LDAStools/frameCPP.py*
%{python_sitearch}/LDAStools/frgetvect_compat.py*
%{python_sitearch}/LDAStools/_frameCPP*.so*

%files framecpp-devel
%defattr(-,root,root)
%exclude %{_libdir}/libframecppc.*a
%{_includedir}/framecpp
%{_libdir}/libframecpp*.*a
%{_libdir}/libframecpp*.so
%{_libdir}/pkgconfig/framecpp_common.pc
%{_libdir}/pkgconfig/framecpp3.pc
%{_libdir}/pkgconfig/framecpp4.pc
%{_libdir}/pkgconfig/framecpp6.pc
%{_libdir}/pkgconfig/framecpp7.pc
%{_libdir}/pkgconfig/framecpp8.pc
%{_libdir}/pkgconfig/framecpp.pc

%files framecpp-doc
%defattr(-,root,root)
%doc %{_docdir}/framecpp

%files framecppc
%defattr(-,root,root,-)
%{_libdir}/libframecppc.so.*

%files framecppc-devel
%defattr(-,root,root)
%{_includedir}/framecppc
%{_libdir}/libframecppc.*a
%{_libdir}/libframecppc.so
%{_libdir}/pkgconfig/framecppc.pc

%files filters
%defattr(-,root,root,-)
%{_libdir}/libfilters*.so.*

%files filters-devel
%defattr(-,root,root)
%{_includedir}/filters
%{_docdir}/filters
%{_libdir}/libfilters*.*a
%{_libdir}/libfilters*.so
%{_libdir}/pkgconfig/filters.pc

%files genericAPI
%defattr(-,root,root,-)
%{_libdir}/libStat*.so.*
%{_libdir}/libldasgen*.so.*
%{_libexecdir}/ldas/lstat

%files genericAPI-python
%defattr(-,root,root,-)
%{python_sitearch}/LDAStools/LDASlogging.py*
%{python_sitearch}/LDAStools/_LDASlogging*.so*

%files genericAPI-devel
%defattr(-,root,root)
%{_includedir}/genericAPI
%{_libdir}/libStat.*a
%{_libdir}/libStat*.so
%{_libdir}/libldasgen.*a
%{_libdir}/libldasgen*.so
%{_docdir}/ldasgen

%files frameAPI
%defattr(-,root,root,-)
%{_libdir}/ldas-tools/libldasframe*.so*

%files frameAPI-python
%defattr(-,root,root,-)
%{python_sitearch}/LDAStools/LDASframe.py*
%{python_sitearch}/LDAStools/_LDASframe*.so*

%files frameAPI-devel
%defattr(-,root,root)
%{_includedir}/frameAPI
%{_libdir}/ldas-tools/libldasframe*.*a
%{_docdir}/frameutils

%files diskcacheAPI
%defattr(-,root,root,-)
%{_bindir}/diskcache
%{_bindir}/ldas-cache-dump-verify
%{_libdir}/ldas-tools/libdiskcache*.so.*
%{_sysconfdir}/initd.d/diskcached
%{_docdir}/diskcache_poller
%{_docdir}/diskcache_server

%files diskcacheAPI-python
%defattr(-,root,root,-)
%{python_sitearch}/LDAStools/diskCache.py*
%{python_sitearch}/LDAStools/_diskCache.so*
%{python_sitearch}/LDAStools/Stream.py*
%{python_sitearch}/LDAStools/_Stream.so*
%{python_sitearch}/LDAStools/libdiskcacheAPI_python.so*

%files diskcacheAPI-devel
%defattr(-,root,root)
%{_includedir}/diskcacheAPI
%{_libdir}/ldas-tools/libdiskcache.*a
%{_libdir}/ldas-tools/libdiskcache*.so
%{_docdir}/diskcache

%files utilities
%{_bindir}/ldas_create_rds

%changelog
* Tue Oct 11 2011 Edward Maros <emaros@ligo.caltech.edu> - ldas-tools-1
- Initial build.
