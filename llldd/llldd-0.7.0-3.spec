# Don't generate any debuginfo packages
%global debug_package %{nil}
%define prefix /usr
%define llservice llldd
%define usrtitle "Low latency data distribution infrastructure"

# Add user/group macro
%define add_user_group() \
  getent group lluser >/dev/null || groupadd -r lluser; \
  getent passwd lluser >/dev/null || useradd -r -g lluser -d %{_datadir}/%{name} -s /sbin/nologin -c %usrtitle lluser; \
%{nil}

# Install default configuration file macro
%define install_default_config() \
  if [ ! -f %{_sysconfdir}/%{1} ];then \
     cp %{_sysconfdir}/%{1}.default %{_sysconfdir}/%{1}; \
  fi; \
%{nil}

# Stop service macro
%define stop_service() \
  /sbin/service %1 stop >/dev/null 2>&1; \
  /sbin/chkconfig --del %1; \
%{nil}

Summary: Ligo low-latency data distribution server initialization
Name: llldd
Version: 0.7.0
Release: 3
License: GPL
Group: Ligo daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: 	gds-services gds-devel virgoApp-Fd >= 6.9.2
BuildRequires: fftw-devel zlib-devel libframe-devel
Requires:  gds-services >= 2.15.7

%description
Init files for Ligo low-latency data distribution system

%package monit
Version: 0.7.0
Summary: Ligo low-latency service monitoring
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: monit

%description monit
Configuration script for monitoring Ligo low-latency services

%package send
Version: 0.7.0
Summary: Ligo low-latency transmission (llsend) service
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: gds-services >= 2.15.7 virgoApp-Fd >= 6.9.2

%description send
Init files for Ligo low-latency data transmisison (llsend) service

%package xfer
Version: 0.7.0
Summary: Ligo low-latency transfer service (llxfer)
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: gds-services >= 2.16.10 virgoApp-Fd >= 6.9.2

%description xfer
Init files for Ligo low-latency data transfer service (llxfer) 

%package ganglia
Version: 0.7.0
Summary: Ligo low-latency data ganglia plugin
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
BuildRequires: apr-devel ganglia-devel libconfuse-devel
Requires: gds-core >= 2.16.10

%description ganglia
Ganglia plugin module and configuration for low-latency monitoring.
 
%package config-cit
Version: 0.7.0
Summary: Ligo Low-latency configuration for Caltech
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: llldd >= 0.7.0
 
%description config-cit
Configuration files for low latency data distribution at Caltech

%package config-lho
Version: 0.7.0
Summary: Ligo Low-latency configuration for LHO
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: llldd >= 0.7.0

%description config-lho
Configuration files for low latency data distribution at LHO

%package config-llo
Version: 0.7.0
Summary: Ligo Low-latency configuration for LLO
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: llldd >= 0.7.0

%description config-llo
Configuration files for low latency data distribution at LLO

%package config-uwm
Version: 0.7.0
Summary: Ligo Low-latency configuration for UWM
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: llldd >= 0.7.0

%description config-uwm
Configuration files for low latency data distribution at UWM

%prep
%setup -q

%build
./configure --prefix=%{prefix} --sysconfdir=/etc --libdir=%{prefix}/%{_lib} \
            --with-user=lluser --enable-ganglia-plugin
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%pre
%add_user_group
[ -d /usr/share/llldd ] || mkdir -p /usr/share/llldd
if [ ! -x  /usr/share/llldd/app_daemon ]; then
  echo "#! /bin/sh"   > /usr/share/llldd/app_daemon
  echo 'cat $1 >> $2 &' >> /usr/share/llldd/app_daemon
  echo 'exit 0'      >> /usr/share/llldd/app_daemon
  chmod +x /usr/share/llldd/app_daemon
fi
exit 0

%pre xfer
%add_user_group
exit 0

%pre send
%add_user_group
exit 0

%post
/sbin/chkconfig --add %llservice
%install_default_config %{llservice}/llshm_config 

%post send
/sbin/chkconfig --add llsend
%install_default_config %{llservice}/llsend_config

%post xfer
/sbin/chkconfig --add llxfer
%install_default_config %{llservice}/llxfer_config

%preun
if [ $1 -eq 0 ]; then
   %stop_service  %llservice
fi

%files
%defattr(-,root,root,-)
# %doc
%{_sysconfdir}/init.d/%{llservice}
%{_sysconfdir}/sysconfig/%{llservice}.default
%{_sysconfdir}/%{llservice}/llshm_config.default

%files monit
%defattr(-,root,root,-)
%{_bindir}/monit_ll_config

%files send
%{_sysconfdir}/init.d/llsend
%{_sysconfdir}/%{llservice}/llsend_config.default
%{_sysconfdir}/%{llservice}/*.cfg
%{_bindir}/DMTtoFd

%preun send
if [ $1 -eq 0 ]; then
   %stop_service llsend
fi

%files xfer
%{_sysconfdir}/init.d/llxfer
%{_sysconfdir}/%{llservice}/llxfer_config.default
%{_bindir}/FdtoDMT

%preun xfer
if [ $1 -eq 0 ]; then
    %stop_service llxfer
fi

%files ganglia
%{_sysconfdir}/ganglia/conf.d/*
%{prefix}/%{_lib}/ganglia/*
%{_bindir}/llmonit

%files config-cit
%{_sysconfdir}/%{llservice}/llshm_config.cit
%{_sysconfdir}/%{llservice}/llxfer_config.cit
%{_sysconfdir}/%{llservice}/llsend_config.cit
%{_sysconfdir}/sysconfig/%{llservice}.cit
%{_sysconfdir}/sysconfig/%{llservice}.cit.ldas-grid

%post config-cit
for srvc in llshm llxfer llsend; do
    [ -f %{_sysconfdir}/%{llservice}/${srvc}_config ] || \
    cp  %{_sysconfdir}/%{llservice}/${srvc}_config.cit \
	%{_sysconfdir}/%{llservice}/${srvc}_config
done
if [ `uname -n` = "ldas-grid" ]; then
    cp %{_sysconfdir}/sysconfig/%{llservice}.cit.ldas-grid \
       %{_sysconfdir}/sysconfig/%{llservice}
else
    cp %{_sysconfdir}/sysconfig/%{llservice}.cit \
       %{_sysconfdir}/sysconfig/%{llservice}
fi

%files config-lho
%{_sysconfdir}/%{llservice}/llshm_config.lho
%{_sysconfdir}/%{llservice}/llxfer_config.lho
%{_sysconfdir}/%{llservice}/llsend_config.lho
%{_sysconfdir}/sysconfig/%{llservice}.lho

%post config-lho
for srvc in llshm llxfer llsend; do
    [ -f %{_sysconfdir}/%{llservice}/${srvc}_config ] || \
    cp  %{_sysconfdir}/%{llservice}/${srvc}_config.lho \
	%{_sysconfdir}/%{llservice}/${srvc}_config
done
[ -f %{_sysconfdir}/sysconfig/%{llservice} ] || \
cp %{_sysconfdir}/sysconfig/%{llservice}.lho \
   %{_sysconfdir}/sysconfig/%{llservice}

%files config-llo
%{_sysconfdir}/%{llservice}/llshm_config.llo
%{_sysconfdir}/%{llservice}/llxfer_config.llo
%{_sysconfdir}/%{llservice}/llsend_config.llo
%{_sysconfdir}/sysconfig/%{llservice}.llo

%post config-llo
for srvc in llshm llxfer llsend; do
    [ -f %{_sysconfdir}/%{llservice}/${srvc}_config ] || \
    cp  %{_sysconfdir}/%{llservice}/${srvc}_config.llo \
	%{_sysconfdir}/%{llservice}/${srvc}_config
done
[ -f %{_sysconfdir}/sysconfig/%{llservice} ] || \
cp %{_sysconfdir}/sysconfig/%{llservice}.llo \
   %{_sysconfdir}/sysconfig/%{llservice}

%files config-uwm
%{_sysconfdir}/%{llservice}/llshm_config.uwm
%{_sysconfdir}/%{llservice}/llxfer_config.uwm
%{_sysconfdir}/sysconfig/%{llservice}.uwm

%post config-uwm
for srvc in llshm llxfer; do
    [ -f %{_sysconfdir}/%{llservice}/${srvc}_config ] || \
    cp  %{_sysconfdir}/%{llservice}/${srvc}_config.uwm \
	%{_sysconfdir}/%{llservice}/${srvc}_config
done
[ -f %{_sysconfdir}/sysconfig/%{llservice} ] || \
cp %{_sysconfdir}/sysconfig/%{llservice}.uwm \
   %{_sysconfdir}/sysconfig/%{llservice}

%changelog
* Wed May 20 2015  <john.zweizig@ligo.org> -
- Add UWM configuration package
- Add inter-site exchange between LLO and LHO

* Wed Apr 23 2015  <john.zweizig@ligo.org> - 
- Separate llsend from llxfer service
- Add monit configuration
- Use macros.

* Tue Jan 10 2012  <ed.maros@ligo.org> - 
- Add.

* Mon Dec 19 2011  <john.zweizig@ligo.org> - 
- Initial build.
