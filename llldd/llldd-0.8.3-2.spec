# Don't generate any debuginfo packages
%global debug_package %{nil}
%define prefix /usr
%define llservice llldd
%define release 2
%define userhome %{_datadir}/%{name}
%define usrtitle "Low latency data distribution infrastructure"

# decide on whether to include virgo
%define withvirgo 1
%if %{withvirgo}
# %define config_virgo --with-virgoapps
%define config_virgo --with-virgoapps
%else
%define config_virgo --without-virgoapps
%endif

# decide on whether to include ganglia
%define mitgang 1
%if %{mitgang}
%define config_ganglia --enable-ganglia
%else
%define config_ganglia --disable-ganglia
%endif

# Add user/group macro
%define add_user_group() \
  getent group lluser >/dev/null || groupadd -r lluser; \
  getent passwd lluser >/dev/null || useradd -r -g lluser -d %{userhome} -s /sbin/nologin -c %usrtitle lluser; \
%{nil}

# Stop service macro
%define stop_service() \
  /sbin/service %1 stop >/dev/null 2>&1; \
  /sbin/chkconfig --del %1; \
%{nil}

Summary: Ligo low-latency data distribution server initialization
Name: llldd
Version: 0.8.3
Release: %{release}%{?dist}
License: GPL
Group: Ligo daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: gds-services gds-devel
BuildRequires: fftw-devel zlib-devel
Requires:  gds-services >= 2.17.6

%description
Init files for Ligo low-latency data distribution system

%package monit
Version: 0.8.3
Summary: Ligo low-latency service monitoring
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: monit

%description monit
Configuration script for monitoring Ligo low-latency services

%package send
Version: 0.8.3
Summary: Ligo low-latency transmission (llsend) service
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: gds-services >= 2.15.7

%description send
Init files for Ligo low-latency data transmisison (llsend) service

%package xfer
Version: 0.8.3
Summary: Ligo low-latency transfer service (llxfer)
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: gds-services >= 2.16.10

%description xfer
Init files for Ligo low-latency data transfer service (llxfer) 
%if %{mitgang}
%package ganglia
Version: 0.8.3
Summary: Ligo low-latency data ganglia plugin
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
BuildRequires: apr-devel ganglia-devel libconfuse-devel
Requires: gds-core >= 2.16.10

%description ganglia
Ganglia plugin module and configuration for low-latency monitoring.
%endif

%package inject
Version: 0.8.3
Summary: Ligo low-latency data injection scripts
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: gds-core >= 2.16.10 gstlal

%description inject
low-latency signal injection scripts

%package nagios
Version: 0.8.3
Summary: Ligo low-latency data nagios scripts
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: gds-core >= 2.16.10 nrpe

%description nagios
nagios scripts and configuration for low-latency monitoring.

%if %{withvirgo}
%package virgo
Version: 0.8.3
Summary: Ligo low-latency virgo data transfer
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: gds-core >= 2.16.10 virgoApp-Fd virgoApp-Cm virgoApp-CSet libframe
BuildRequires: virgoApp-Fd-devel >= 8.15.2 libframe-devel

%description virgo
Virgo low latency data interface utilities
%endif


%package config-cit
Version: 0.8.3
Summary: Ligo Low-latency configuration for Caltech
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: llldd >= 0.8.3
 
%description config-cit
Configuration files for low latency data distribution at Caltech

%package config-lho
Version: 0.8.3
Summary: Ligo Low-latency configuration for LHO
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: llldd >= 0.8.3

%description config-lho
Configuration files for low latency data distribution at LHO

%package config-llo
Version: 0.8.3
Summary: Ligo Low-latency configuration for LLO
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: llldd >= 0.8.3

%description config-llo
Configuration files for low latency data distribution at LLO

%package config-uwm
Version: 0.8.3
Summary: Ligo Low-latency configuration for UWM
Group: LIGO daswg
URL: https://wiki.ligo.org/daswg/LowLatency
Requires: llldd >= 0.8.3

%description config-uwm
Configuration files for low latency data distribution at UWM

%prep
%setup -q

%build
./configure --prefix=%{prefix} --sysconfdir=/etc --libdir=%{prefix}/%{_lib} \
            --with-user=lluser %{config_ganglia} %{config_virgo}
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%pre
%add_user_group
[ -d %{userhome} ] || mkdir -p %{userhome}
if [ ! -x  %{userhome}/app_daemon ]; then
  echo "#! /bin/sh"   > %{userhome}/app_daemon
  echo 'cat $1 >> $2 &' >> %{userhome}/app_daemon
  echo 'exit 0'      >> %{userhome}/app_daemon
  chmod +x %{userhome}/app_daemon
fi
exit 0

%pre xfer
%add_user_group
exit 0

%pre send
%add_user_group
exit 0

%post
/sbin/chkconfig --add %llservice

%post send
/sbin/chkconfig --add llsend

%post xfer
/sbin/chkconfig --add llxfer

%preun
if [ $1 -eq 0 ]; then
   %stop_service  %llservice
fi

%files
%defattr(-,root,root,-)
# %doc
%{_sysconfdir}/init.d/%{llservice}
%{_sysconfdir}/sysconfig/%{llservice}.default
%{_sysconfdir}/%{llservice}/llshm_config.default
%config %{_sysconfdir}/security/limits.d/*

%files monit
%defattr(-,root,root,-)
%{_bindir}/monit_ll_config
%{_bindir}/llmonit

%files send
%{_sysconfdir}/init.d/llsend
%{_sysconfdir}/%{llservice}/llsend_config.default
%{_sysconfdir}/%{llservice}/*.cfg

%preun send
if [ $1 -eq 0 ]; then
   %stop_service llsend
fi

%files xfer
%{_sysconfdir}/init.d/llxfer
%{_sysconfdir}/%{llservice}/llxfer_config.default

%preun xfer
if [ $1 -eq 0 ]; then
    %stop_service llxfer
fi

%if %{withvirgo}
%files virgo
%{_bindir}/DMTtoFd
%{_bindir}/FdtoDMT
%{_sysconfdir}/%{llservice}/CmDomains
%endif

%if %{mitgang}
%files ganglia
%{_sysconfdir}/ganglia/conf.d/*
%{prefix}/%{_lib}/ganglia/*
%endif

%files inject
%{_sysconfdir}/init.d/inject-H1
%{_bindir}/ftee-mod

%post inject
ln -fs %{_sysconfdir}/init.d/inject-H1 %{_sysconfdir}/init.d/inject-L1
ln -fs %{_sysconfdir}/init.d/inject-H1 %{_sysconfdir}/init.d/inject-V1

%files nagios
%{userhome}/scripts/smsource_test
%{_sysconfdir}/nrpe.d/low-latency-cit.cfg
%{_sysconfdir}/nrpe.d/low-latency-lho.cfg
%{_sysconfdir}/nrpe.d/low-latency-llo.cfg
%{prefix}/%{_lib}/nagios/plugins/custom/check_inject
%{prefix}/%{_lib}/nagios/plugins/custom/dmt_shm_stat

%files config-cit
%config(noreplace) %{_sysconfdir}/%{llservice}/llshm_config.cit
%config(noreplace) %{_sysconfdir}/%{llservice}/llxfer_config.cit
%config(noreplace) %{_sysconfdir}/%{llservice}/llsend_config.cit
%config(noreplace) %{_sysconfdir}/sysconfig/%{llservice}.cit

%post config-cit
for srvc in llshm llxfer llsend; do
    cp  %{_sysconfdir}/%{llservice}/${srvc}_config.cit \
	%{_sysconfdir}/%{llservice}/${srvc}_config
done
cp %{_sysconfdir}/sysconfig/%{llservice}.cit \
   %{_sysconfdir}/sysconfig/%{llservice}

%files config-lho
%config(noreplace) %{_sysconfdir}/%{llservice}/llshm_config.lho
%config(noreplace) %{_sysconfdir}/%{llservice}/llxfer_config.lho
%config(noreplace) %{_sysconfdir}/%{llservice}/llsend_config.lho
%config(noreplace) %{_sysconfdir}/sysconfig/%{llservice}.lho

%post config-lho
for srvc in llshm llxfer llsend; do
    cp  %{_sysconfdir}/%{llservice}/${srvc}_config.lho \
	%{_sysconfdir}/%{llservice}/${srvc}_config
done
cp %{_sysconfdir}/sysconfig/%{llservice}.lho \
   %{_sysconfdir}/sysconfig/%{llservice}

%files config-llo
%config(noreplace) %{_sysconfdir}/%{llservice}/llshm_config.llo
%config(noreplace) %{_sysconfdir}/%{llservice}/llxfer_config.llo
%config(noreplace) %{_sysconfdir}/%{llservice}/llsend_config.llo
%config(noreplace) %{_sysconfdir}/sysconfig/%{llservice}.llo

%post config-llo
for srvc in llshm llxfer llsend; do
    cp  %{_sysconfdir}/%{llservice}/${srvc}_config.llo \
	%{_sysconfdir}/%{llservice}/${srvc}_config
done
cp %{_sysconfdir}/sysconfig/%{llservice}.llo \
   %{_sysconfdir}/sysconfig/%{llservice}

%files config-uwm
%config(noreplace) %{_sysconfdir}/%{llservice}/llshm_config.uwm
%config(noreplace) %{_sysconfdir}/%{llservice}/llxfer_config.uwm
%config(noreplace) %{_sysconfdir}/sysconfig/%{llservice}.uwm

%post config-uwm
for srvc in llshm llxfer; do
    cp  %{_sysconfdir}/%{llservice}/${srvc}_config.uwm \
	%{_sysconfdir}/%{llservice}/${srvc}_config
done
cp %{_sysconfdir}/sysconfig/%{llservice}.uwm \
   %{_sysconfdir}/sysconfig/%{llservice}

%changelog
* Thu Mar 24 2016  <john.zweizig@ligo.org> -
- Use limits.conf to allow Dpush to lock memory and set rt priority
- new configurations for lowlatency machines

* Wed May 20 2015  <john.zweizig@ligo.org> -
- Add UWM configuration package
- Add inter-site exchange between LLO and LHO

* Wed Apr 22 2015  <john.zweizig@ligo.org> - 
- Separate llsend from llxfer service
- Add monit configuration
- Use macros.

* Tue Jan 10 2012  <ed.maros@ligo.org> - 
- Add.

* Mon Dec 19 2011  <john.zweizig@ligo.org> - 
- Initial build.
