%define 	name pyxmpp
%define 	version 1.0.1
%define 	release 3

Summary: 	XMPP/Jabber implementation for Python
Name: 		%{name}
Version: 	%{version}
Release: 	%{release}%{?dist}
Source0: 	%{name}-%{version}.tar.gz
License: 	LGPL
Group: 		Development/Libraries
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires: 	python python-dns python-kerberos m2crypto libxml2
BuildRequires:  python python-devel libxml2-devel
Prefix: 	%{_prefix}
Vendor: 	Jacek Konieczny <jajcus@jajcus.net>
Url: 		http://pyxmpp.jajcus.net/

%description
This is a Python implementation of the Extensible Messaging and Presence Protocol -XMPP- (http://xmpp.org/protocols/) formerly known as Jabber protocol.

%prep
%setup

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
python setup.py install -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)

%changelog
* Wed Apr 7 2010 Xavier Amador <xavier.amador@gravity.phys.uwm.edu> 1.0.1-1.lscsoft
- Creation and re-building (first building in June 24/2009) for LSCOSFT LIGO Scientific Collaboration

