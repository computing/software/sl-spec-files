%define    __installdir /etc/lscsoft-test

Name:      lscsoft-test
Version:   1.0
Release:   1
Summary:   Simple test package for testing build infrastructure
License:   GPL
Group:     LSCSoft
Source:    %{name}-%{version}.tar.xz
URL:       http://ligo-vcs.phys.uwm.edu/cgit/sl-metapackages/tree/lscsoft-test
Packager:  Adam Mercer <adam.mercer@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires:  yum
BuildArch: noarch

%description
A very simple test package for testing the package building
infrastructure

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}/%{__installdir}
%{__install} lscsoft-test.txt %{buildroot}/%{__installdir}

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -rf ${RPM_BUILD_ROOT}
rm -rf ${RPM_BUILD_DIR}/%{name}-%{version}

%files
%defattr(0644,root,root)
%{__installdir}/lscsoft-test.txt

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Thu Mar 05 2015 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial version
