%define 	name 	python-voeventlib
%define 	version 0.3
%define 	release 1

Summary: 	reference implementation and parser for VOEvent2 [International Virtual Observatory Alliance (IVOA)]
Name: 		%{name}
Version: 	%{version}
Release: 	%{release}%{?dist}
Source0: 	%{name}-%{version}.tar.gz
License: 	LGPL
Group: 		Development/Libraries
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires: 	python-lxml
BuildRequires:  python
Prefix: 	%{_prefix}
Vendor: 	"Roy Williams" <roy@caltech.edu> <roy.williams@ligo.org>, "Dave Kuhlmann" 
Url: 		http://lib.skyalert.org/VOEventLib/

%description
This is the reference implementation and parser for the VOEvent2 XML specification 
[International Virtual Observatory Alliance (IVOA)], which is a standardized language 
used to report observations of astronomical events.

%prep
%setup

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
python setup.py install -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)

%changelog
* Thu Oct 18 2012 Xavier Amador <xavier.amador@gravity.phys.uwm.edu> 0.3-1.lscsoft
- First building for LSCSoft LIGO Scientific Collaboration

