%define gstreamername gstreamer_lscsoft

Name: gstlal-inspiral
Version: 0.6.1
Release: 3.lscsoft
Summary: GSTLAL Experimental Supplements
License: GPL
Group: LSC Software/Data Analysis
Requires: gstlal >= 0.10.0 gstlal-ugly >= 0.9.0 python >= 2.6 glue >= 1.49 glue-segments >= 1.49 python-pylal >= 0.8.0 %{gstreamername} >= 0.10.32 %{gstreamername}-plugins-base >= 0.10.32 %{gstreamername}-plugins-good >= 0.10.27 gstreamer_lscsoft-python >= 0.10.21 h5py pygobject2 numpy scipy lal >= 6.14.0 lalmetaio >= 1.2.6 lalinspiral >= 1.6.5 gsl ligo-gracedb >= 1.11
BuildRequires: doxygen graphviz gstlal-devel >= 0.10.0 python-devel >= 2.6 %{gstreamername}-devel >= 0.10.32 %{gstreamername}-plugins-base-devel >= 0.10.32 pygobject2-devel lal-devel >= 6.14.0 lalmetaio-devel >= 1.2.6 lalinspiral-devel >= 1.6.5 gsl-devel
Source: gstlal-inspiral-%{version}.tar.gz
URL: https://www.lsc-group.phys.uwm.edu/daswg/projects/gstlal.html
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains plugins, libraries, and programs for inspiral data
analysis.


%package devel
Summary: Files and documentation needed for compiling gstlal-inspiral based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version} gstlal-devel >= 0.10.0 python-devel >= 2.6 %{gstreamername}-devel >= 0.10.32 %{gstreamername}-plugins-base-devel >= 0.10.32 pygobject2-devel lal-devel >= 6.14.0 lalmetaio-devel >= 1.2.6 lalinspiral-devel >= 1.6.5 gsl-devel
%description devel
This package contains the files needed for building gstlal-inspiral based
plugins and programs.


%prep
%setup -q -n %{name}-%{version}


%build
. /opt/lscsoft/gst/gstenvironment.sh
# FIXME:  enable doxygen when we can rely on a platform with a new-enough
# version.  don't forget to uncomment the documentation entries in the
# files section
%configure --without-doxygen
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gstlal/*
# commented out until RPM-based reference platform can be relied upon to
# have a new-enough doxygen installed
#%{_docdir}/gstlal-inspiral-*
%{_libdir}/*.so.*
%{_libdir}/gstreamer-0.10/*.so
%{_libdir}/gstreamer-0.10/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_libdir}/gstreamer-0.10/*.a
%{_includedir}/*
