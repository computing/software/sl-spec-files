%define gstreamername gstreamer1

Name: gstlal-inspiral
Version: 0.99.0
Release: 2%{?dist}
Summary: GSTLAL Experimental Supplements
License: GPL
Group: LSC Software/Data Analysis
Requires: gstlal >= 0.99.4
Requires: gstlal-ugly >= 0.99.2
Requires: python >= 2.7
Requires: glue >= 1.50
Requires: glue-segments >= 1.50
Requires: python-pylal >= 0.9.0
Requires: %{gstreamername} >= 1.2.4
Requires: %{gstreamername}-plugins-base >= 1.2.4
Requires: %{gstreamername}-plugins-good >= 1.2.4
Requires: %{gstreamername}-plugins-bad-free
Requires: h5py
Requires: numpy
Requires: scipy
Requires: lal >= 6.15.2
Requires: lal-python >= 6.15.2
Requires: lalmetaio >= 1.2.6
Requires: lalinspiral >= 1.7.0
Requires: lalinspiral-python >= 1.7.0
Requires: gsl
Requires: ligo-gracedb >= 1.11
Requires: python-%{gstreamername}
BuildRequires: doxygen >= 1.8.3
BuildRequires: graphviz
BuildRequires: gstlal-devel >= 0.99.4
BuildRequires: python-devel >= 2.7
BuildRequires: %{gstreamername}-devel >= 1.2.4
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.2.4
BuildRequires: lal-devel >= 6.15.2
BuildRequires: lal-python >= 6.15.2
BuildRequires: lalmetaio-devel >= 1.2.6
BuildRequires: lalinspiral-devel >= 1.7.0
BuildRequires: lalinspiral-python >= 1.7.0
BuildRequires: gsl-devel
Source: gstlal-inspiral-%{version}.tar.gz
URL: https://wiki.ligo.org/DASWG/GstLAL
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains plugins, libraries, and programs for inspiral data
analysis.


%package devel
Summary: Files and documentation needed for compiling gstlal-inspiral based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version} gstlal-devel >= 0.99.4 python-devel >= 2.7 %{gstreamername}-devel >= 1.2.4 %{gstreamername}-plugins-base-devel >= 1.2.4 lal-devel >= 6.15.2 lalmetaio-devel >= 1.2.6 lalinspiral-devel >= 1.7.0 gsl-devel
%description devel
This package contains the files needed for building gstlal-inspiral based
plugins and programs.


%prep
%setup -q -n %{name}-%{version}


%build
%configure --enable-gtk-doc
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gstlal/*
%{_docdir}/gstlal-inspiral-*
%{_libdir}/*.so.*
%{_libdir}/gstreamer-*/*.so
#%{_libdir}/gstreamer-*/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_libdir}/gstreamer-*/*.a
%{_includedir}/*
