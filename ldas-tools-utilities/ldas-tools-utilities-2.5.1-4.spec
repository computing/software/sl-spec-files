# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define _docdir %{_datadir}/doc/ldas-tools

Summary: LDAS tools utility programs
Name: ldas-tools-utilities
Version: 2.5.1
Release: 4%{?dist}
License: ?
URL: http://www.ligo.caltech.edu
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: http://software.ligo.org/lscsoft/source/ldas-tools-utilities-%{version}.tar.gz
Requires: ldas-tools-diskcacheAPI
Requires: ldas-tools-frameAPI
Buildrequires: autoconf
Buildrequires: automake
Buildrequires: doxygen
Buildrequires: libtool
Buildrequires: pkgconfig
Buildrequires: ldas-tools-ldasgen-devel >= 2.4.99
Buildrequires: ldas-tools-diskcacheAPI-devel >= 2.4.99
Buildrequires: ldas-tools-frameAPI-devel >= 2.4.99

%description
This provides utilities that are useful to end users

%prep

%setup -q

%build

#------------------------------------------------------------------------
# This works around a bug in the current rpmbuild system whereby the
#   PKG_CONFIG_PATH is set by the system and does not allow for
#   user preference.
# This work around should be fixed in RH 7.1 or so
#------------------------------------------------------------------------
export PKG_CONFIG_PATH="${LDASTOOLSDEV_PKG_CONFIG_PATH:-}${LDASTOOLSDEV_PKG_CONFIG_PATH:+:}$PKG_CONFIG_PATH"

%configure --disable-warnings-as-errors --with-optimization=high --disable-tcl --enable-python --docdir=%{_docdir}
make V=1 %{?_smp_mflags}
make V=1 check

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;
rm -f %{buildroot}%{python_sitearch}/LDAStools/_*.a
rm -f %{buildroot}%{python_sitearch}/LDAStools/libdiskcacheAPI_python.a

%post
ldconfig
%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%{_bindir}/ldas_create_rds

%changelog
* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <emaros@ligo.caltech.edu> - 1.19.13-1
- Initial build.
