%define nightly %{nil}
%define _prefix /usr
%define _mandir %{_prefix}/share/man
%define _sysconfdir %{_prefix}/etc
%define _pkgpythondir %{_prefix}/lib64/python2.?/site-packages/lalapps
%define _pkgpyexecdir %{_libdir}/python2.?/site-packages/lalapps
%define release 2

%if "%{?nightly:%{nightly}}%{!?nightly:0}" == "%{nil}"
%undefine nightly
%endif

Name: lalapps
Version: 6.23.0
Release: %{?nightly:0.%{nightly}}%{!?nightly:%{release}}%{?dist}
Summary: LSC Algorithm Library Applications
License: GPLv2+
Group: LAL
Source: %{name}-%{version}%{?nightly:-%{nightly}}.tar.xz
URL: https://wiki.ligo.org/Computing/DASWG/LALSuite
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildRequires: cfitsio-devel
BuildRequires: fftw-devel
BuildRequires: gsl-devel
BuildRequires: less
BuildRequires: libframe-devel
BuildRequires: libmetaio-devel
BuildRequires: openmpi-devel
BuildRequires: lal-devel >= 6.19.2
BuildRequires: python2-lal >= 6.19.2
BuildRequires: lalframe-devel >= 1.4.0
BuildRequires: python2-lalframe >= 1.4.0
BuildRequires: lalmetaio-devel >= 1.5.0
BuildRequires: python2-lalmetaio >= 1.5.0
BuildRequires: lalsimulation-devel >= 1.8.2
BuildRequires: python2-lalsimulation >= 1.8.2
BuildRequires: lalburst-devel >= 1.5.1
BuildRequires: python2-lalburst >= 1.5.1
BuildRequires: lalinspiral-devel >= 1.8.1
BuildRequires: python2-lalinspiral >= 1.8.1
BuildRequires: lalpulsar-devel >= 1.17.1
BuildRequires: python2-lalpulsar >= 1.17.1
BuildRequires: lalinference-devel >= 1.10.2
BuildRequires: python2-lalinference >= 1.10.2
Requires: cfitsio
Requires: fftw
Requires: gsl
Requires: h5py
Requires: healpy
Requires: less
Requires: libframe
Requires: libmetaio
Requires: openmpi
Requires: openssh-clients
Requires: python
Requires: lal >= 6.19.2
Requires: python2-lal >= 6.19.2
Requires: lalframe >= 1.4.0
Requires: python2-lalframe >= 1.4.0
Requires: lalmetaio >= 1.5.0
Requires: python2-lalmetaio >= 1.5.0
Requires: lalsimulation >= 1.8.2
Requires: python2-lalsimulation >= 1.8.2
Requires: lalburst >= 1.5.1
Requires: python2-lalburst >= 1.5.1
Requires: lalinspiral >= 1.8.1
Requires: python2-lalinspiral >= 1.8.1
Requires: lalpulsar >= 1.17.1
Requires: python2-lalpulsar >= 1.17.1
Requires: lalinference >= 1.10.2
Requires: python2-lalinference >= 1.10.2
Requires: python2-ligo-segments
Obsoletes: lalxml < 1.2.5-1
Obsoletes: lalxml-debuginfo < 1.2.5-1
Obsoletes: lalxml-devel < 1.2.5-1
Obsoletes: lalxml-python < 1.2.5-1
Obsoletes: lalxml-python3 < 1.2.5-1
Obsoletes: lalxml-octave < 1.2.5-1
Obsoletes: laldetchar < 0.3.6-1
Obsoletes: laldetchar-debuginfo < 0.3.6-1
Obsoletes: laldetchar-devel < 0.3.6-1
Obsoletes: laldetchar-python < 0.3.6-1
Obsoletes: laldetchar-octave < 0.3.6-1
Obsoletes: lalstochastic < 1.1.21-1
Obsoletes: lalstochastic-debuginfo < 1.1.21-1
Obsoletes: lalstochastic-devel < 1.1.21-1
Obsoletes: lalstochastic-python < 1.1.21-1
Obsoletes: lalstochastic-python3 < 1.1.21-1
Obsoletes: lalstochastic-octave < 1.1.21-1
Provides: lalxml = 1.2.5-1
Provides: lalxml-debuginfo = 1.2.5-1
Provides: lalxml-devel = 1.2.5-1
Provides: lalxml-python = 1.2.5-1
Provides: lalxml-python3 = 1.2.5-1
Provides: lalxml-octave = 1.2.5-1
Provides: laldetchar = 0.3.6-1
Provides: laldetchar-debuginfo = 0.3.6-1
Provides: laldetchar-devel = 0.3.6-1
Provides: laldetchar-python = 0.3.6-1
Provides: laldetchar-octave = 0.3.6-1
Provides: lalstochastic = 1.1.21-1
Provides: lalstochastic-debuginfo = 1.1.21-1
Provides: lalstochastic-devel = 1.1.21-1
Provides: lalstochastic-python = 1.1.21-1
Provides: lalstochastic-python3 = 1.1.21-1
Provides: lalstochastic-octave = 1.1.21-1

Prefix: %{_prefix}

%description
The LSC Algorithm Library Applications for gravitational wave data analysis.
This package contains applications that are built on tools in the LSC
Algorithm Library.

%prep
%setup -q -n %{name}-%{version}%{?nightly:-%{nightly}}

%build
# force linking agsinst system libmpi
export OMPI_LIBS="-lmpi -Wl,--disable-new-dtags"
%configure --disable-gcc-flags --enable-cfitsio --enable-openmp --enable-mpi MPICC=/usr/lib64/openmpi/bin/mpicc MPICXX=/usr/lib64/openmpi/bin/mpicxx MPIFC=/usr/lib64/openmpi/bin/mpifc
%{__make} %{?_smp_mflags} V=1

%check
%{__make} %{?_smp_mflags} V=1 VERBOSE=1 check

%install
%make_install

%post
ldconfig

%postun
ldconfig

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}%{?nightly:-%{nightly}}

%files
%defattr(-,root,root)
%license COPYING
%{_bindir}/*
%{_datadir}/lalapps/*
%{_mandir}/man1/*
%{_pkgpythondir}/*
%{_sysconfdir}/lalapps-user-env.*

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Mon Feb 25 2019 Adam Mercer <adam.mercer@ligo.org> 6.23.0-1
- ER14 release

* Thu Sep 13 2018 Adam Mercer <adam.mercer@ligo.org> 6.22.0-1
- Pre O3 release

* Tue Feb 07 2017 Adam Mercer <adam.mercer@ligo.org> 6.21.0-1
- O2 release

* Mon Sep 26 2016 Adam Mercer <adam.mercer@ligo.org> 6.20.0-1
- ER10 release

* Thu Jun 23 2016 Adam Mercer <adam.mercer@ligo.org> 6.19.0-1
- ER9 release

* Fri Mar 25 2016 Adam Mercer <adam.mercer@ligo.org> 6.18.0-1
- Pre O2 packaging test release
