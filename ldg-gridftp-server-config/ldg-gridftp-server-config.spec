%define _prefix /etc/sysconfig

Name: ldg-gridftp-server-config
Version: 1.1
Release: 1
Group: LDG
License: BSD / Globus
Summary: LDG GridFTP Server Configuration
Packager: Adam Mercer <adam.mercer@ligo.org>
Source:   %{name}-%{version}.tar.gz
Requires: globus-gridftp-server-progs
BuildArch: noarch

%description
This package provides the configuration for the Globus GridFTP Server.

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}%{_prefix}/
install -m 644 ./globus-gridftp-server %{buildroot}%{_prefix}/

%files
%defattr(-,root,root)
%{_prefix}/globus-gridftp-server
