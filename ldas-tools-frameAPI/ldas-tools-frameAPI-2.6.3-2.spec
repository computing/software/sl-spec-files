# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define _docdir %{_datadir}/doc/ldas-tools-%{version}

Summary: LDAS tools libframeAPI toolkit runtime files
Name: ldas-tools-frameAPI
Version: 2.6.3
Release: 2%{?dist}
License: GPLv2+
URL: http://www.ligo.caltech.edu
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: http://software.ligo.org/lscsoft/source/ldas-tools-frameAPI-%{version}.tar.gz
Requires: ldas-tools-framecpp >= 2.6.4
Requires: ldas-tools-filters >= 2.6.3
Requires: ldas-tools-ldasgen >= 2.6.3
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: gawk
BuildRequires: make
BuildRequires: rpm-build
Buildrequires: autoconf
Buildrequires: automake
Buildrequires: doxygen
Buildrequires: ldas-tools-al-devel
Buildrequires: ldas-tools-ldasgen
Buildrequires: libtool
Buildrequires: openssl-devel
Buildrequires: pkgconfig
Buildrequires: zlib-devel
Buildrequires: ldas-tools-framecpp-devel >= 2.6.4
Buildrequires: ldas-tools-filters-devel >= 2.6.3
Buildrequires: ldas-tools-ldasgen-devel >= 2.6.3

%description
This provides the runtime libraries for the frameAPI library.

%package devel
Group: Development/Scientific
Summary: LDAS tools libframeAPI toolkit development files
Requires: ldas-tools-al-devel >= 2.6.2
Requires: ldas-tools-filters-devel >= 2.6.3
Requires: ldas-tools-frameAPI = %{version}
%description devel
This provides the develpement files the frameAPI library.

%prep

%setup -q

%build

#------------------------------------------------------------------------
# In newer versions of automake, this should be
#   AM_DISTCHECK_CONFIGURE_FLAGS
# so as to allow user to overwrite the value on the command line.
# Currenly using DISTCHECK_CONFIGURE_FLAGS for compatability with
# very old versions of automake.
#------------------------------------------------------------------------
export PKG_CONFIG_PATH="${LDASTOOLSDEV_PKG_CONFIG_PATH:-}${LDASTOOLSDEV_PKG_CONFIG_PATH:+:}$PKG_CONFIG_PATH"

%configure --disable-warnings-as-errors --with-optimization=high --docdir=%{_docdir}
make V=1 %{?_smp_mflags}
make V=1 check

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;

%post
ldconfig

%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/libldasframe*.so*

%files devel
%defattr(-,root,root)
%{_includedir}/frameAPI
%{_libdir}/libldasframe*.*a
%{_docdir}/frameutils
%{_libdir}/pkgconfig/ldastools-ldasframe.pc

%changelog
* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.3-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.2-1
- Built for new release

* Sat Oct 22 2016 Edward Maros <ed.maros@ligo.org> - 2.5.2-1
- Built for new release

* Fri Sep 09 2016 Edward Maros <ed.maros@ligo.org> - 2.5.1-1
- Built for new release

* Thu Apr 07 2016 Edward Maros <ed.maros@ligo.org> - 2.5.0-1
- Built for new release

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <ed.maros@ligo.org> - 1.19.13-1
- Initial build.
