# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define _docdir %{_datadir}/doc/ldas-tools-%{version}

Summary: LDAS tools libframeAPI toolkit runtime files
Name: ldas-tools-frameAPI
Version: 2.5.1
Release: 2%{?dist}
License: ?
URL: http://www.ligo.caltech.edu
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: http://software.ligo.org/lscsoft/source/ldas-tools-frameAPI-%{version}.tar.gz
Requires: ldas-tools-al
Buildrequires: ldas-tools-ldasgen
Buildrequires: autoconf
Buildrequires: automake
Buildrequires: bison
Buildrequires: doxygen
Buildrequires: flex
Buildrequires: ldas-tools-al-devel
Buildrequires: ldas-tools-ldasgen
Buildrequires: libtool
Buildrequires: openssl-devel
Buildrequires: pkgconfig
Buildrequires: python-devel
Buildrequires: swig
Buildrequires: zlib-devel
Buildrequires: ldas-tools-framecpp-devel >= 2.4.99
Buildrequires: ldas-tools-framecpp-python >= 2.4.99
Buildrequires: ldas-tools-filters-devel >= 2.4.99
Buildrequires: ldas-tools-ldasgen-devel >= 2.4.99

%description
This provides the runtime libraries for the frameAPI library.

%package python
Group: Application/Scientific
Summary: LDAS tools libframeAPI toolkit runtime files
Requires: ldas-tools-frameAPI
%description python
This provides the libraries needed to utilize the frameAPI library
from within Python

%package devel
Group: Development/Scientific
Summary: LDAS tools libframeAPI toolkit development files
Requires: ldas-tools-al-devel
Requires: ldas-tools-filters-devel
Requires: ldas-tools-frameAPI
%description devel
This provides the develpement files the frameAPI library.

%prep

%setup -q

%build

#------------------------------------------------------------------------
# In newer versions of automake, this should be
#   AM_DISTCHECK_CONFIGURE_FLAGS
# so as to allow user to overwrite the value on the command line.
# Currenly using DISTCHECK_CONFIGURE_FLAGS for compatability with
# very old versions of automake.
#------------------------------------------------------------------------
export PKG_CONFIG_PATH="${LDASTOOLSDEV_PKG_CONFIG_PATH:-}${LDASTOOLSDEV_PKG_CONFIG_PATH:+:}$PKG_CONFIG_PATH"

%configure --disable-warnings-as-errors --with-optimization=high --disable-tcl --enable-python --docdir=%{_docdir}
make V=1 %{?_smp_mflags}
make V=1 check

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;
rm -f %{buildroot}%{python_sitearch}/LDAStools/_*.a

%post
ldconfig

%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/libldasframe*.so*

%files python
%defattr(-,root,root,-)
%{python_sitearch}/LDAStools/LDASframe.py*
%{python_sitearch}/LDAStools/_LDASframe*.so*

%files devel
%defattr(-,root,root)
%{_includedir}/frameAPI
%{_libdir}/libldasframe*.*a
%{_docdir}/frameutils
%{_libdir}/pkgconfig/ldastools-ldasframe.pc

%post python
/bin/touch %{python_sitearch}/LDAStools/__init__.py

%postun python
if [ -d %{python_sitearch}/LDAStools ];
then
        if [ `ls -1 %{python_sitearch}/LDAStools | wc -l` -le 1 ];
        then
                rm -rf %{python_sitearch}/LDAStools
        fi
fi

%changelog
* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <ed.maros@ligo.org> - 1.19.13-1
- Initial build.
