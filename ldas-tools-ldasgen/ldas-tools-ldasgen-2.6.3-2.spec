# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define _docdir %{_datadir}/doc/ldas-tools-%{version}

Summary: LDAS tools libgenericAPI toolkit runtime files
Name: ldas-tools-ldasgen
Version: 2.6.3
Release: 2%{?dist}
License: GPLv2+
URL: http://www.ligo.caltech.edu
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: http://software.ligo.org/lscsoft/source/ldas-tools-ldasgen-%{version}.tar.gz
Obsoletes: ldas-tools-genericAPI
Requires: ldas-tools-al >= 2.6.2
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: gawk
BuildRequires: make
BuildRequires: rpm-build
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: doxygen
BuildRequires: flex
BuildRequires: libtool
BuildRequires: boost-devel
BuildRequires: openssl-devel
BuildRequires: pkgconfig
BuildRequires: zlib-devel
BuildRequires: ldas-tools-al-devel >= 2.6.2

%description
This provides the runtime libraries for the genericAPI library.

%package devel
Group: Development/Scientific
Summary: LDAS tools libgenericAPI toolkit development files
Obsoletes: ldas-tools-genericAPI-devel
Requires: ldas-tools-al-devel >= 2.6.2
Requires: ldas-tools-ldasgen = %{version}
Requires: boost-devel
%description devel
This provides the develpement files the genericAPI library.

%prep

%setup -q

%build

#------------------------------------------------------------------------
# This works around a bug in the current rpmbuild system whereby the
#   PKG_CONFIG_PATH is set by the system and does not allow for
#   user preference.
# This work around should be fixed in RH 7.1 or so
#------------------------------------------------------------------------
export PKG_CONFIG_PATH="${LDASTOOLSDEV_PKG_CONFIG_PATH:-}${LDASTOOLSDEV_PKG_CONFIG_PATH:+:}$PKG_CONFIG_PATH"

%configure --disable-warnings-as-errors --with-optimization=high --docdir=%{_docdir}
make V=1 %{?_smp_mflags}
make V=1 check

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;

%post
ldconfig
%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/libStat*.so.*
%{_libdir}/libldasgen*.so.*
%{_libexecdir}/ldas/lstat

%files devel
%defattr(-,root,root)
%{_includedir}/genericAPI
%{_libdir}/libStat.*a
%{_libdir}/libStat*.so
%{_libdir}/libldasgen.*a
%{_libdir}/libldasgen*.so
%{_docdir}/ldasgen
%{_libdir}/pkgconfig/ldastools-ldasgen.pc
%{_libdir}/pkgconfig/ldastools-stat.pc

%changelog
* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.3-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.2-1
- Built for new release

* Sat Oct 22 2016 Edward Maros <ed.maros@ligo.org> - 2.5.4-1
- Built for new release

* Mon Oct 10 2016 Edward Maros <ed.maros@ligo.org> - 2.5.3-1
- Built for new release

* Mon Sep 26 2016 Edward Maros <ed.maros@ligo.org> - 2.5.2-1
- Expanded information reported for failed stat and lstat calls

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <ed.maros@ligo.org> - 1.19.13-1
- Initial build.
