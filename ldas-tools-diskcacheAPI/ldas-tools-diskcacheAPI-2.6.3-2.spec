# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define _docdir %{_datadir}/doc/ldas-tools-%{version}

Summary: LDAS tools libdiskcacheAPI toolkit runtime files
Name: ldas-tools-diskcacheAPI
Version: 2.6.3
Release: 2%{?dist}
License: GPLv2+
URL: http://www.ligo.caltech.edu
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: http://software.ligo.org/lscsoft/source/ldas-tools-diskcacheAPI-%{version}.tar.gz
Requires: ldas-tools-al >= 2.6.2
Requires: ldas-tools-ldasgen >= 2.6.3
Requires(pre): shadow-utils
%{?systemd_requires}
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: gawk
BuildRequires: make
BuildRequires: rpm-build
Buildrequires: autoconf
Buildrequires: automake
Buildrequires: doxygen
Buildrequires: libtool
Buildrequires: openssl-devel
Buildrequires: pkgconfig
Buildrequires: zlib-devel
Buildrequires: ldas-tools-ldasgen-devel >= 2.6.3
Buildrequires: systemd

%description
This provides the runtime libraries for the diskcacheAPI library.

%package devel
Group: Development/Scientific
Summary: LDAS tools libdiskcacheAPI toolkit development files
Requires: ldas-tools-ldasgen-devel >= 2.6.3
Requires: ldas-tools-diskcacheAPI = %{version}
%description devel
This provides the develpement files the diskcacheAPI library.

%prep

%setup -q

%build

#------------------------------------------------------------------------
# This works around a bug in the current rpmbuild system whereby the
#   PKG_CONFIG_PATH is set by the system and does not allow for
#   user preference.
# This work around should be fixed in RH 7.1 or so
#------------------------------------------------------------------------
export PKG_CONFIG_PATH="${LDASTOOLSDEV_PKG_CONFIG_PATH:-}${LDASTOOLSDEV_PKG_CONFIG_PATH:+:}$PKG_CONFIG_PATH"

%configure --disable-warnings-as-errors --with-optimization=high --docdir=%{_docdir}
make V=1 %{?_smp_mflags}
make V=1 check

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;

mkdir -p %{buildroot}%{_sharedstatedir}/diskcache
mkdir -p %{buildroot}/var/log/diskcache

# Remove the init script that has been replaced by a systemd unit file
rm -f %{buildroot}/etc/initd.d/diskcached

%pre
getent group diskcache >/dev/null || groupadd -r diskcache
getent passwd diskcache >/dev/null || \
    useradd -r -g diskcache -d %{_sharedstatedir}/diskcache -s /sbin/nologin \
    -c "Dedicated diskcache service account" diskcache
exit 0

%post
/sbin/ldconfig
%systemd_post diskcache.service

%preun
%systemd_preun diskcache.service

%postun
/sbin/ldconfig
%systemd_postun diskcache.service

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_sysconfdir}/diskcache
%attr(0755,diskcache,diskcache) %dir /var/log/diskcache
%attr(0755,diskcache,diskcache) %dir %{_sharedstatedir}/diskcache

%{_bindir}/diskcache
%{_bindir}/ldas-cache-dump-verify
%{_libdir}/libdiskcache*.so.*
%{_unitdir}/diskcache.service
%{_sysconfdir}/diskcache/diskcache.rsc.sample
%{_docdir}/diskcache_poller
%{_docdir}/diskcache_server

%files devel
%defattr(-,root,root)
%{_includedir}/diskcacheAPI
%{_libdir}/libdiskcache.*a
%{_libdir}/libdiskcache*.so
%{_docdir}/diskcache
%{_libdir}/pkgconfig/ldastools-diskcache.pc

%changelog
* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.3-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.2-1
- Built for new release

* Sat Oct 22 2016 Edward Maros <ed.maros@ligo.org> - 2.5.5-1
- Built for new release

* Mon Oct 10 2016 Edward Maros <ed.maros@ligo.org> - 2.5.4-1
- Built for new release

* Mon Sep 26 2016 Edward Maros <ed.maros@ligo.org> - 2.5.3-1
- Fixed diskcache to recover from devices going offline (#4645)

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <ed.maros@ligo.org> - 1.19.13-1
- Initial build.
