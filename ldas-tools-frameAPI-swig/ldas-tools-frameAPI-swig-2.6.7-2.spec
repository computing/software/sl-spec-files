# -*- mode: rpm-spec; indent-tabs-mode: nil -*-
#========================================================================
%define    basename_   ldas-tools-frameAPI
%define    name        %{basename_}-swig
%define    name_desc   frame utility
%define    hdr_dir     frameAPI
%define    tarbasename ldas-tools-frameAPI-swig
%define    version     2.6.7
%define    release     2
%define    _docdir     %{_datadir}/doc/%{name}-%{version}
%if 0%{!?python3_pkgversion}
%if 0%{?python3_version_nodots}
%define python3_pkgversion %{python3_version_nodots}
%else
%define python3_pkgversion 34
%endif
%endif
%define    python_build_opts  -DENABLE_SWIG_PYTHON2=yes -DPYTHON2_EXECUTABLE=%{__python2}
%define    python3_build_opts -DENABLE_SWIG_PYTHON3=yes -DPYTHON3_EXECUTABLE=%{__python3}
%if %{?cmake3:1}%{!?cmake3:0}
%define __cmake %__cmake3
%define cmake %cmake3
%define ctest ctest3
%else
%define ctest ctest
%endif

Name:      %{name}
Summary:   SWIG bindings for LDAS Tools frame utility library
Version:   %{version}
Release:   %{release}%{?dist}
License:   GPLv2+
URL:       http://www.ligo.caltech.edu
Group:     Application/Scientific
Provides:  %{name} = %{version}
Obsoletes: %{name} < %{version}
Conflicts: %{basename_}-devel < 2.6.0
Prefix:    %_prefix
BuildRoot: %{buildroot}
Source0:   http://software.ligo.org/lscsoft/source/%{tarbasename}-%{version}.tar.gz
#------------------------------------------------------------------------
%if 0%{?rhl} <= 7 || 0%{?sl7} <= 7
BuildRequires: cmake3 >= 3.6
BuildRequires: cmake
%else
BuildRequires: cmake >= 3.6
%endif
BuildRequires: python34
BuildRequires: python3-rpm-macros
BuildRequires: gawk
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: make
Buildrequires: ldas-tools-cmake >= 1.0.5
Buildrequires: %{basename_}-devel >= 2.6.3
Buildrequires: pkgconfig
BuildRequires: rpm-build
BuildRequires: swig
BuildRequires: ldas-tools-al-swig >= 2.6.6
BuildRequires: ldas-tools-framecpp-swig >= 2.6.6
BuildRequires: ldas-tools-ldasgen-swig >= 2.6.6
#........................................................................
# Python dependencies
#........................................................................
BuildRequires: python
BuildRequires: python-devel
#........................................................................
# Python 3 dependencies
#........................................................................
BuildRequires: python%{python3_pkgversion}
BuildRequires: python%{python3_pkgversion}-devel
#........................................................................
# Runtime dependencies
#........................................................................
Requires: swig
Requires: ldas-tools-al-swig >= 2.6.6
Requires: ldas-tools-framecpp-swig >= 2.6.6
Requires: ldas-tools-ldasgen-swig >= 2.6.6
Requires: %{basename_}-devel >= 2.6.3

%description


#========================================================================
%package -n python2-%{basename_}
Summary: LDAS Tools %{name_desc} toolkit python %{python_version} bindings
#........................................................................
# Runtime dependencies
#........................................................................
Requires: python2-ldas-tools-framecpp >= 2.6.4
Requires: python2-ldas-tools-ldasgen >= 2.6.3
Requires: %{basename_} >= 2.6.3
Obsoletes: %{basename_}-python < %{version}

%description -n  python2-%{basename_}
This provides the python%{python_version} bindings for the %{name_desc} library

#========================================================================
%package -n python%{python3_pkgversion}-%{basename_}
Summary: LDAS Tools %{name_desc} toolkit python %{python3_version} bindings
#........................................................................
# Runtime dependencies
#........................................................................
Requires: python%{python3_pkgversion}-ldas-tools-framecpp >= 2.6.4
Requires: python%{python3_pkgversion}-ldas-tools-ldasgen >= 2.6.3
Requires: %{basename_} >= 2.6.3

%description -n  python%{python3_pkgversion}-%{basename_}
This provides the python%{python3_version} bindings for the %{name_desc} library

#========================================================================

%prep
%setup -c -T -D -a 0 -n %{name}-%{version}

%build
%cmake \
    %{python_build_opts} \
    %{python3_build_opts} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    %{tarbasename}-%{version}

%install
make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_docdir}/README

%check
%ctest -V %{?_smp_mflags}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/%{hdr_dir}/*.i

%files -n python2-%{basename_}
%defattr(-,root,root,-)
%{_libdir}/python2*

%files -n python%{python3_pkgversion}-%{basename_}
%defattr(-,root,root,-)
%{_libdir}/python3*

%changelog
* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.7-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.6-1
- Built for new release

* Tue Oct 11 2011 Edward Maros <emaros@ligo.caltech.edu> - 1.19.13-1
- Initial build.
