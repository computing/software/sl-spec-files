%define gstreamername gstreamer_lscsoft

Name: gstlal
Version: 0.9.0
Release: 2.lscsoft
Summary: GSTLAL
License: GPL
Group: LSC Software/Data Analysis
Requires: python >= 2.6 glue >= 1.48 glue-segments >= 1.48 python-pylal >= 0.7.0 fftw >= 3 %{gstreamername} >= 0.10.32 %{gstreamername}-plugins-base >= 0.10.32 %{gstreamername}-plugins-good >= 0.10.27 %{gstreamername}-python >= 0.10.21 pygobject2 numpy scipy lal >= 6.14.0 lalmetaio >= 1.2.6 lalsimulation >= 1.3.0 lalburst >= 1.3.0 lalinspiral >= 1.6.5 gsl orc >= 0.4.16
BuildRequires: python-devel >= 2.6 fftw-devel >= 3 %{gstreamername}-devel >= 0.10.32 %{gstreamername}-plugins-base-devel >= 0.10.32 pygobject2-devel lal-devel >= 6.14.0 lalmetaio-devel >= 1.2.6 lalsimulation-devel >= 1.3.0 lalburst-devel >= 1.3.0 lalinspiral-devel >= 1.6.5 gsl-devel orc >= 0.4.16 numpy
Source: gstlal-%{version}.tar.gz
URL: https://www.lsc-group.phys.uwm.edu/daswg/projects/gstlal.html
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains the plugins and shared libraries required to run
gstlal-based applications.


%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version} python-devel >= 2.6 fftw-devel >= 3 %{gstreamername}-devel >= 0.10.32 %{gstreamername}-plugins-base-devel >= 0.10.32 pygobject2-devel lal-devel >= 6.14.0 lalmetaio-devel >= 1.2.6 lalsimulation-devel >= 1.3.0 lalburst-devel >= 1.3.0 lalinspiral-devel >= 1.6.5 gsl-devel
%description devel
This package contains the files needed for building gstlal-based plugins
and programs.


%prep
%setup -q -n %{name}-%{version}


%build
. /opt/lscsoft/gst/gstenvironment.sh
%configure
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gstlal
%{_datadir}/gtk-doc/html/gstlal-*
#%{_docdir}/gstlal-*
%{_libdir}/*.so.*
%{_libdir}/gstreamer-0.10/*.so
%{_libdir}/gstreamer-0.10/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_libdir}/gstreamer-0.10/*.a
%{_includedir}/*
