%define gstreamername gstreamer1

Name: gstlal
Version: 1.5.0
Release: 2%{?dist}
Summary: GSTLAL
License: GPL
Group: LSC Software/Data Analysis
Requires: avahi
Requires: avahi-glib
Requires: avahi-ui-tools
Requires: fftw >= 3
Requires: glue >= 2.0.0
Requires: python-ligo-lw >= 1.5.3
Requires: python2-ligo-segments >= 1.2.0
Requires: gobject-introspection >= 1.30.0
Requires: gsl
Requires: %{gstreamername} >= 1.14.1
Requires: %{gstreamername}-plugins-bad-free
Requires: %{gstreamername}-plugins-base >= 1.14.1
Requires: %{gstreamername}-plugins-good >= 1.14.1
# FIXME:  add this when it becomes available, and figure out what its name
# is on .rpm based systems
#Requires: %{gstreamername}-python3-plugin-loader
# FIXME:  add this when it becomes available
#Requires: %{gstreamername}-rtsp-server >= 1.14.1
Requires: lal >= 6.19.0
Requires: lal-python >= 6.19.0
Requires: lalburst >= 1.5.0
Requires: lalmetaio >= 1.4.0
Requires: lalinspiral >= 1.8.0
Requires: lalsimulation >= 1.8.0
Requires: numpy
Requires: orc >= 0.4.16
Requires: python >= 2.7
Requires: python-%{gstreamername}
Requires: python-gobject >= 3.22
Requires: scipy
Requires: zlib
BuildRequires: doxygen >= 1.8.3
BuildRequires: fftw-devel >= 3
BuildRequires: gobject-introspection-devel >= 1.30.0
BuildRequires: graphviz
BuildRequires: gsl-devel
BuildRequires: gtk-doc >= 1.11
BuildRequires: %{gstreamername}-devel >= 1.14.1
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.14.1
# FIXME:  add this when it becomes available
#BuildRequires: %{gstreamername}-rtsp-server-devel >= 1.14.1
BuildRequires: lal-devel >= 6.19.0
BuildRequires: lal-python >= 6.19.0
BuildRequires: lalburst-devel >= 1.5.0
BuildRequires: lalinspiral-devel >= 1.8.0
BuildRequires: lalmetaio-devel >= 1.4.0
BuildRequires: lalsimulation-devel >= 1.8.0
BuildRequires: numpy
BuildRequires: orc >= 0.4.16
BuildRequires: python-devel >= 2.7
# needed for gstpythonplugin.c remove when we remove that plugin from gstlal
BuildRequires: pygobject3-devel >= 3.22
BuildRequires: zlib-devel
Source: gstlal-%{version}.tar.gz
URL: https://wiki.ligo.org/DASWG/GstLAL
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains the plugins and shared libraries required to run
gstlal-based applications.


%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version}
Requires: fftw-devel >= 3
Requires: gsl-devel
Requires: %{gstreamername}-devel >= 1.14.1
Requires: %{gstreamername}-plugins-base-devel >= 1.14.1
Requires: lal-devel >= 6.19.0
Requires: lalmetaio-devel >= 1.4.0
Requires: lalsimulation-devel >= 1.8.0
Requires: lalburst-devel >= 1.5.0
Requires: lalinspiral-devel >= 1.8.0
Requires: python-devel >= 2.7
%description devel
This package contains the files needed for building gstlal-based plugins
and programs.


%prep
%setup -q -n %{name}-%{version}


%build
export LIBS="-lz"
%configure --enable-gtk-doc
%make_build


%install
%make_install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gir-*/*
%{_datadir}/gstlal
%{_datadir}/gtk-doc/html/gstlal-*
%{_docdir}/gstlal-*
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/*
%{_libdir}/gstreamer-1.0/*.so
%{_libdir}/gstreamer-1.0/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/gstreamer-1.0/*.a
%{_libdir}/pkgconfig/*
%{_includedir}/*
