%define gstreamername gstreamer1

Name: gstlal
Version: 0.99.4
Release: 2%{?dist}
Summary: GSTLAL
License: GPL
Group: LSC Software/Data Analysis
Requires: python >= 2.7
Requires: glue >= 1.50
Requires: glue-segments >= 1.50
Requires: python-pylal >= 0.9.0
Requires: fftw >= 3
Requires: %{gstreamername} >= 1.2.4
Requires: %{gstreamername}-plugins-base >= 1.2.4
Requires: %{gstreamername}-plugins-good >= 1.2.4
Requires: %{gstreamername}-plugins-bad-free
Requires: numpy
Requires: scipy
Requires: lal >= 6.15.2
Requires: lal-python >= 6.15.2
Requires: lalmetaio >= 1.2.6
Requires: lalsimulation >= 1.4.0
Requires: lalburst >= 1.4.0
Requires: lalinspiral >= 1.7.0
Requires: gsl
Requires: orc >= 0.4.16
Requires: dbus-python
Requires: avahi-ui-tools
Requires: python-%{gstreamername}
BuildRequires: doxygen >= 1.8.3
BuildRequires: graphviz
BuildRequires: python-devel >= 2.7
BuildRequires: gobject-introspection-devel >= 1.30.0
BuildRequires: fftw-devel >= 3
BuildRequires: %{gstreamername}-devel >= 1.2.4
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.2.4
BuildRequires: numpy
BuildRequires: lal-devel >= 6.15.2
BuildRequires: lal-python >= 6.15.2
BuildRequires: lalmetaio-devel >= 1.2.6
BuildRequires: lalsimulation-devel >= 1.4.0
BuildRequires: lalburst-devel >= 1.4.0
BuildRequires: lalinspiral-devel >= 1.7.0
BuildRequires: gsl-devel
BuildRequires: orc >= 0.4.16
BuildRequires: pygobject3-devel
Source: gstlal-%{version}.tar.gz
URL: https://wiki.ligo.org/DASWG/GstLAL
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains the plugins and shared libraries required to run
gstlal-based applications.


%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version} python-devel >= 2.7 fftw-devel >= 3 %{gstreamername}-devel >= 1.2.4 %{gstreamername}-plugins-base-devel >= 1.2.4 lal-devel >= 6.15.2 lalmetaio-devel >= 1.2.6 lalsimulation-devel >= 1.4.0 lalburst-devel >= 1.4.0 lalinspiral-devel >= 1.7.0 gsl-devel
%description devel
This package contains the files needed for building gstlal-based plugins
and programs.


%prep
%setup -q -n %{name}-%{version}


%build
%configure
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gir-1.0/*.gir
%{_datadir}/gstlal
%{_datadir}/gtk-doc/html/gstlal-*
%{_docdir}/gstlal-*
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/*
%{_libdir}/gstreamer-1.0/*.so
%{_libdir}/gstreamer-1.0/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_libdir}/gstreamer-1.0/*.a
%{_includedir}/*
