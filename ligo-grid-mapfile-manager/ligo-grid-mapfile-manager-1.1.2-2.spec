Summary: LIGO Grid-Mapfile Manager
Name: ligo-grid-mapfile-manager
Version: 1.1.2
Release: 2%{?dist}
Source0: ligo-grid-mapfile-manager-%{version}.tar.gz
License: Unknown
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Url: https://wiki.ligo.org/AuthProject
Requires: python >= 2.6, python-ldap, monit, logrotate, ca-certificates

%description
%{summary}

%prep
%setup -q 

%build

%install
# install the executables
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/sbin
install --mode=0755 lgmm $RPM_BUILD_ROOT/usr/sbin/lgmm
install --mode=0755 normalize_grid-mapfile $RPM_BUILD_ROOT/usr/sbin/normalize_grid-mapfile

# install configuration template
mkdir -p $RPM_BUILD_ROOT/etc/lgmm
install --mode=0644 lgmm_config.py $RPM_BUILD_ROOT/etc/lgmm/lgmm_config.py

# install monit configuration
mkdir -p $RPM_BUILD_ROOT/etc/monit.d
install --mode=0644 lgmm_monit $RPM_BUILD_ROOT/etc/monit.d/lgmm_monit

# install log rotation configuration
mkdir -p $RPM_BUILD_ROOT/etc/logrotate.d
install --mode=0644 debian/lgmm.logrotate $RPM_BUILD_ROOT/etc/logrotate.d/lgmm

# install defaults template
mkdir -p $RPM_BUILD_ROOT/etc/default 
install --mode=0644 debian/ligo-grid-mapfile-manager.lgmm.default $RPM_BUILD_ROOT/etc/default/lgmm

# install the init script
mkdir -p $RPM_BUILD_ROOT/etc/init.d
install --mode=0755 lgmm.init.sl6 $RPM_BUILD_ROOT/etc/init.d/lgmm

%post
/sbin/chkconfig --add lgmm > /dev/null 2>&1

mkdir -p /var/log/lgmm > /dev/null 2>&1
chown nobody /var/log/lgmm > /dev/null 2>&1

mkdir -p /var/run/lgmm > /dev/null 2>&1
chown nobody /var/run/lgmm > /dev/null 2>&1
chmod 755 /var/run/lgmm > /dev/null 2>&1

%preun
if [ $1 = 0 ]; then
    /sbin/service lgmm stop > /dev/null 2>&1
    /sbin/chkconfig --del lgmm > /dev/null 2>&1
fi

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0755,root,root,-)
/usr/sbin/lgmm
/usr/sbin/normalize_grid-mapfile
/etc/init.d/lgmm
%config(noreplace) /etc/monit.d/lgmm_monit
%config(noreplace) /etc/logrotate.d/lgmm
%config(noreplace) /etc/default/lgmm
%config(noreplace) /etc/lgmm/lgmm_config.py
%attr(0644,root,root) /etc/monit.d/lgmm_monit
%attr(0644,root,root) /etc/logrotate.d/lgmm
%attr(0644,root,root) /etc/default/lgmm
%attr(0644,root,root) /etc/lgmm/lgmm_config.py
%exclude /etc/lgmm/lgmm_config.pyc
%exclude /etc/lgmm/lgmm_config.pyo

%changelog
* Tue May 12 2015 Paul Hopkins <paul.hopkins@ligo.org> - 1.1.2-0
- Only use default usernames if site specific ones do not exist.

* Wed Oct 15 2014 Paul Hopkins <paul.hopkins@ligo.org> - 1.1.1-0
- Prevent inclusion of potentially dangerous grid-subjects.

* Fri Mar 29 2013 Scott Koranda <scott.koranda@ligo.org> - 1.1.0-1
- Added features and no start at boot time.

* Thu Mar 28 2013 Scott Koranda <scott.koranda@ligo.org> - 1.0.3-1
- Fixed sorting issue with account names

* Mon Mar 25 2013 Scott Koranda <scott.koranda@ligo.org> - 1.0.2-1
- Still better init script functionality

* Sun Mar 24 2013 Scott Koranda <scott.koranda@ligo.org> - 1.0.1-1
- Better init script and daemon functionality

* Tue Mar 19 2013 Scott Koranda <scott.koranda@ligo.org> - 1.0.0-1
- Initial version.
