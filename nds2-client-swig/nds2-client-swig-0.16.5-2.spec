#========================================================================
# NOTES:
#    The octave default is to check for a system supplied package.
#
# OCTIVE override:
#    You can specify to build without octive support by specifying
#      --without octive
#    when invoking rpmbuild.
#========================================================================
%define namespace nds2-client
%define name     nds2-client-swig
%define version  0.16.5
%define release  2
%define _prefix  /usr
%define _includedir %{_prefix}/include/%{namespace}
%define _pkgdocdir  %{_defaultdocdir}/%{name}

%if 0%{!?python3_pkgversion}
%if 0%{?python3_version_nodots}
%define python3_pkgversion %{python3_version_nodots}
%else
%define python3_pkgversion 34
%endif
%endif

%define _use_internal_dependency_generator 0
%define __find_requires %{_builddir}/%{name}-%{version}/config/nds-find-requires

#========================================================================
#
#  Set up octave symbols as appropriate
#
#========================================================================
%if %{?_without_octave:1}%{!?_without_octave:0}
%define octave_installed %(echo 0)
%else
%define octave_installed %(test -e /usr/bin/octave && echo 1 || echo 0)
%endif

%define octave_build_opts %{nil}

%if 0%{octave_installed}
%define _octdatadir %(octave-config --m-site-dir)
%define _octexecdir %(octave-config --oct-site-dir)
%define octave_build_opts -DENABLE_SWIG_OCTAVE=yes -Dpkgoctdatadir=%_octdatadir -DOCTAVE_INSTALL_DIR=%_octexecdir
%endif

#========================================================================
#
#           Set up python symbols as appropriate
#
#========================================================================
%define python_installed %(echo 1)
%define python_build_opts  -DENABLE_SWIG_PYTHON2=yes -DPYTHON2_EXECUTABLE=%{__python2}
%define python3_installed %(echo 1)
%define python3_build_opts -DENABLE_SWIG_PYTHON3=yes -DPYTHON3_EXECUTABLE=%{__python3}

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Sanity checks
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#========================================================================
# Main spec file
#========================================================================
Name:           %{name}
Summary:        NDS2 Client interface
Version:        %{version}
Release:        %{release}%{?dist}
License:        GPL
Group:          LSC Software/Data Analysis
Source:         http://software.ligo.org/lscsoft/source/%{name}-%{version}.tar.gz
Packager:       Edward Maros (ed.maroso@ligo.org)
URL:            https://wiki.ligo.org/DASWG/NDSClient
BuildRoot:      %{buildroot}
BuildRequires:  gcc, gcc-c++, glibc
BuildRequires:  libstdc++-static
%if 0%{?rhl} <= 7 || 0%{?sl7} <= 7
BuildRequires:  cmake3 >= 3.6
BuildRequires:  cmake >= 2.6
%else
BuildRequires:  cmake >= 3.6
%endif
BuildRequires:  make
BuildRequires:  doxygen, graphviz
BuildRequires:  rpm-build
BuildRequires:  python3-rpm-macros
BuildRequires:  gawk, pkgconfig
BuildRequires:  cyrus-sasl-devel, cyrus-sasl-gssapi
BuildRequires:  libcurl-devel
BuildRequires:  nds2-client-devel >= 0.15.3
BuildRequires:  swig >= 3.0.7
BuildRequires:  python-devel, numpy
BuildRequires:  python%{python3_pkgversion}-devel, python%{python3_pkgversion}-numpy
BuildRequires:  java-1.7.0-openjdk-devel, octave-devel
BuildRequires:  octave
Provides:       %{name} = %{version}
Obsoletes:      %name < %version
Prefix:         %_prefix

%description
The NDS2 client interface allow the user to down-load LIGO data from V1 and V2 LIGO Network Data Servers.

%package -n %{namespace}-java
Group: Development/Scientific
Summary: Java extensions for NDS2
%description -n %{namespace}-java
This provides java wrappers for the nds2 client

%package -n %{namespace}-matlab
Group: Development/Scientific
Summary: MATLAB extensions for NDS2
Requires: %{namespace}-java = %{version}
%description -n %{namespace}-matlab
This provides MATLAB wrappers for the nds2 client

%package -n %{namespace}-octave
Group: Development/Scientific
Summary: Octave extensions for NDS2
%description -n %{namespace}-octave
This provides extensions to octave to access an NDS2 server

%package -n python2-%{namespace}
Group: Development/Scientific
Summary: Python extensions for NDS2
Requires: python
Requires: numpy
Obsoletes: %{namespace}-python <= 0.15.3
%description -n python2-%{namespace}
This provides python wrappers for the nds2 client

%package -n python%{python3_pkgversion}-%{namespace}
Group: Development/Scientific
Summary: Python extensions for NDS2
Requires: python%{python3_pkgversion}
Requires: python%{python3_pkgversion}-numpy
%description -n python%{python3_pkgversion}-%{namespace}
This provides python wrappers for the nds2 client

%package doc
Group: Development/Scientific
Summary: NDS2 client documentation files
Requires: %{name} = %{version}
BuildArch: noarch
%description doc
This package contains the doxygen formated pages for the nds2-client package and its
high-level language extensions.

%package all
Group:     Development/Scientific
Summary:   NDS2 Client interface
BuildArch: noarch
Requires:  %{name} = %{version}
Requires:  %{name}-doc = %{version}
Requires:  %{namespace}-java = %{version}
Requires:  %{namespace}-matlab = %{version}
Requires:  %{namespace}-octave = %{version}
Requires:  python2-%{namespace} = %{version}
Requires:  python%{python3_pkgversion}-%{namespace} = %{version}
%description all
 The Network Data Server (NDS) is a TCP/IP protocol for retrieving
 online or archived data from thousands of instrument channels at LIGO
 (Laser Interferometer Gravitational-Wave Observatory) sites and data
 analysis clusters.  Version 2 of the protocol includes communication
 with Kerberos and GSSAPI.
 .
 This package installs all NDS2 client packages, including libraries,
 language bindings, binary interface, and documentation.

#----------------------------------------------
# Get onto the fun of building the NDS software
#----------------------------------------------

%prep
%setup -q

%build
%if %{?cmake3:1}%{!?cmake3:0}
%cmake3 %{octave_build_opts} %{python_build_opts} %{python3_build_opts} -DWITH_SASL=yes -DWITH_GSSAPI=no -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 .
%define ctest ctest3
%else
%cmake %{octave_build_opts} %{python_build_opts} %{python3_build_opts} -DWITH_SASL=yes -DWITH_GSSAPI=no -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 .
%define ctest ctest
%endif

%install
make install DESTDIR=%{buildroot}

%check
%ctest -V %{?_smp_mflags}

#----------------------------------------------
# Remove files that will not be packaged based
# on the arch type
#----------------------------------------------

#----------------------------------------------
# Do the noarch files
#----------------------------------------------

%files doc
%{_docdir}

#----------------------------------------------
# Handle binary packages
#----------------------------------------------

%files
%{_includedir}

#------------------------------------------------------------------------
# Java
#------------------------------------------------------------------------
%files -n %{namespace}-java
%_libdir/java/nds2
%_libdir/%{namespace}/java
%_datadir/%{namespace}/java
%_sysconfdir/%{namespace}/%{namespace}-java.cfg

#------------------------------------------------------------------------
# Octave
#------------------------------------------------------------------------
%if %{octave_installed}
%files -n %{namespace}-octave
%{_octexecdir}
%_sysconfdir/%{namespace}/%{namespace}-octave.cfg
%endif

#------------------------------------------------------------------------
# Python
#------------------------------------------------------------------------
%if %{python_installed}
%files -n python2-%{namespace}
%{python_sitearch}/
%_sysconfdir/%{namespace}/%{namespace}-py2*.cfg
%endif

#------------------------------------------------------------------------
# Python 3
#------------------------------------------------------------------------
%if %{python3_installed}
%files -n python%{python3_pkgversion}-%{namespace}
%{python3_sitearch}/
%_sysconfdir/%{namespace}/%{namespace}-py%{python3_pkgversion}*.cfg
%endif

#------------------------------------------------------------------------
# MATLAB
#------------------------------------------------------------------------
%files -n %{namespace}-matlab
%_datadir/%{namespace}/matlab
%_sysconfdir/%{namespace}/%{namespace}-matlab.cfg

#------------------------------------------------------------------------
# All - metapackage
#------------------------------------------------------------------------
%files all
