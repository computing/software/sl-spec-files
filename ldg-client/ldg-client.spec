Name: ldg-client
Version: 5.2.1
Release: 4%{?dist}
Group: LDG
License: BSD / Globus
Summary: LDG Client Packages
Packager: Adam Mercer <adam.mercer@ligo.org>
Requires: globus-gass-copy-progs
Requires: globus-gram-client-tools
Requires: ldg-core
Requires: ldg-upgrade
Requires: ldg-version
Requires: myproxy
Requires: uberftp
Requires: /usr/bin/gsissh
BuildArch: noarch

Obsoletes: ldg-cert-util

%description
This meta-package will pull in all necessary packages to provide the
user with a client configuration to access remote (LDG) clusters as well
as apply for, retrieve, and renew their DoE certificates.

%install

%files

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Wed Mar 09 2016 Adam Mercer <adam.mercer@ligo.org> 5.2.1-4
- use path style dependency for gsissh, supports both SL6 and SL7

* Fri Nov 21 2014 Adam Mercer <adam.mercer@ligo.org> 5.2.1-3
- don't provide ldg-cert-util

* Thu Nov 20 2014 Adam Mercer <adam.mercer@ligo.org> 5.2.1-2
- use multiple Requires lines
- fix changelog order

* Wed Nov 05 2014 Adam Mercer <adam.mercer@ligo.org> 5.2.1-1
- ldg-cert-util is no longer needed, or used

* Wed Nov 02 2011 Adam Mercer <adam.mercer@ligo.org> 5.2-1
- update dependencies for SL6

* Wed Apr 27 2011 Adam Mercer <adam.mercer@ligo.org> 5.1-1
- initial version
