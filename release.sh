#/bin/bash

PACKAGE=$1
VERSION=$2
RELEASETAG=$3

git archive --format=tar --prefix=${PACKAGE}-${VERSION}/ ${RELEASETAG}:${PACKAGE} | xz -z > ${PACKAGE}-${VERSION}.tar.xz
