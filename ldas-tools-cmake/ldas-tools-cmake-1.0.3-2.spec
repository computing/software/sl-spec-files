#========================================================================
#========================================================================
%define name 	ldas-tools-cmake
%define version 1.0.3
%define release 2
%define summary This is a collection of CMake functions used by LDAS Tools
%define _prefix /usr
%define _sysconfdir %{_prefix}/etc
%define _includedir %{_prefix}/include/%{name}
%define _pkgdocdir  %{_defaultdocdir}/%{name}
%if %{?cmake3:1}%{!?cmake3:0}
%define __cmake %__cmake3
%define cmake %cmake3
%define ctest %ctest3
%endif

#========================================================================
# Main spec file
#========================================================================
Name: 		%{name}
Summary: 	%{summary}
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	?
Group: 		LSC Software/Data Analysis
Source: 	%{name}-%{version}.tar.gz
Packager: 	Edward Maros (ed.maroso@ligo.org)
URL: 		https://wiki.ligo.org/DASWG/LDASTools
BuildRoot:      %{buildroot}
%if 0%{?rhl} <= 7 || 0%{?sl7} <= 7
BuildRequires:  cmake3 >= 3.6
BuildRequires:  cmake
%else
BuildRequires:  cmake >= 3.6
%endif
BuildRequires:  rpm-build
BuildRequires:  python3-rpm-macros
BuildRequires:  make
BuildRequires:  gcc, gcc-c++, glibc
BuildRequires:	gawk, pkgconfig
Provides: 	%{name}
Obsoletes:	%{name} < %{version}
Prefix:		%_prefix

%description
This collection of cmake macros was developed as part of;the LDAS Tools Suite to ease the transition from;autotools/automake to CMake.

#------------------------------------------------------------------------
# Get onto the fun of building the NDS software
#------------------------------------------------------------------------

%prep
%setup -q

%build
%cmake -Wno-dev .
%define ctest ctest

%install
%__cmake --build . --target install -- DESTDIR=%{buildroot}

%check
%ctest -V %{?_smp_mflags}


#----------------------------------------------
# Do the noarch files
#----------------------------------------------

%files
%_datadir/
%_libdir/pkgconfig/ldastoolscmake.pc
