# -*- mode: rpm-spec; indent-tabs-mode: nil -*-
#========================================================================
%define    ldas_tools_cmake_ver 1.0.5
%define    ldas_tools_al_ver    2.6.2
%define    basename_            ldas-tools-al
%define    name                 %{basename_}-swig
%define    hdr_dir              ldastoolsal
%define    tarbasename          ldas-tools-al-swig
%define    version              2.6.6
%define    release              2
%define    _docdir              %{_datadir}/doc/%{name}-%{version}

%if 0%{!?python3_pkgversion}
%if 0%{?python3_version_nodots}
%define python3_pkgversion %{python3_version_nodots}
%else
%define python3_pkgversion 34
%endif
%endif
%define    python_build_opts  -DENABLE_SWIG_PYTHON2=yes -DPYTHON2_EXECUTABLE=%{__python2}
%define    python3_build_opts -DENABLE_SWIG_PYTHON3=yes -DPYTHON3_EXECUTABLE=%{__python3}
%if %{?cmake3:1}%{!?cmake3:0}
%define __cmake %__cmake3
%define cmake %cmake3
%define ctest ctest3
%else
%define ctest ctest
%endif

Name:      %{name}
Summary:   LDAS tools abstraction toolkit language buildings
Version:   %{version}
Release:   %{release}%{?dist}
License:   GPLv2+
URL:       http://www.ligo.caltech.edu
Group:     Application/Scientific
Provides:  %{name} = %{version}
Obsoletes: %{name} < %{version}
Conflicts: ldas-tools-al <= 2.6.1
Prefix:    %_prefix
BuildRoot: %{buildroot}
Source0:   http://software.ligo.org/lscsoft/source/%{tarbasename}-%{version}.tar.gz
#------------------------------------------------------------------------
%if 0%{?rhl} <= 7 || 0%{?sl7} <= 7
BuildRequires: cmake3 >= 3.6
BuildRequires: cmake
%else
BuildRequires: cmake >= 3.6
%endif
BuildRequires: python3-rpm-macros
BuildRequires: gawk
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: make
BuildRequires: ldas-tools-cmake >= %{ldas_tools_cmake_ver}
BuildRequires: %{basename_}-devel >= %{ldas_tools_al_ver}
BuildRequires: pkgconfig
BuildRequires: rpm-build
BuildRequires: swig
#........................................................................
# Python dependencies
#........................................................................
BuildRequires: python
BuildRequires: python-devel
#........................................................................
# Python 3 dependencies
#........................................................................
BuildRequires: python%{python3_pkgversion}
BuildRequires: python%{python3_pkgversion}-devel
#........................................................................
# Runtime dependencies
#........................................................................
Requires: %{basename_}-devel

%description
This provides the runtime libraries for the abstaction library.

#========================================================================
%package -n python2-%{basename_}
Summary: LDAS Tools %{name_desc} toolkit python %{python_version} bindings
#........................................................................
# Runtime dependencies
#........................................................................
Requires: ldas-tools-al >= %{ldas_tools_al_version}
Requires: python

%description -n  python2-%{basename_}
This provides the python%{python_version} bindings for the %{name_desc} library

#========================================================================
%package -n python%{python3_version_nodots}-%{basename_}
Summary:   LDAS tools abstraction toolkit language buildings
#........................................................................
# Runtime dependencies
#........................................................................
Requires: ldas-tools-al >= %{ldas_tools_al_ver}
Requires: python%{python3_version_nodots}

%description -n  python%{python3_version_nodots}-%{basename_}
This provides the runtime libraries for the abstaction library.

%prep
%setup -c -T -D -a 0 -n %{name}-%{version}

%cmake \
    %{python_build_opts} \
    %{python3_build_opts} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    %{tarbasename}-%{version}

%install
%__cmake --build . --target install -- DESTDIR=%{buildroot}

%check
%ctest -V %{?_smp_mflags}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/%{hdr_dir}/*.i

%files -n python2-%{basename_}
%defattr(-,root,root,-)
%{_libdir}/python2*

%files -n python%{python3_version_nodots}-%{basename_}
%defattr(-,root,root,-)
%{_libdir}/python3*

%changelog
* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.5-1
- Built for new release

* Tue Oct 11 2011 Edward Maros <emaros@ligo.caltech.edu> - 1.19.13-1
- Initial build.
