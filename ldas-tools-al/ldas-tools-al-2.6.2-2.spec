# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define _docdir %{_datadir}/doc/ldas-tools-al-%{version}

Summary: LDAS tools abstraction toolkit
Name: ldas-tools-al
Version: 2.6.2
Release: 2%{?dist}
License: GPLv2+
URL: http://www.ligo.caltech.edu
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: http://software.ligo.org/lscsoft/source/ldas-tools-al-%{version}.tar.gz
Obsoletes: ldas-tools-general
Requires: openssl
Requires: zlib
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: gawk
BuildRequires: make
BuildRequires: rpm-build
Buildrequires: autoconf
Buildrequires: automake
Buildrequires: libtool
BuildRequires: boost-devel
Buildrequires: openssl-devel
Buildrequires: pkgconfig
Buildrequires: zlib-devel
Buildrequires: doxygen, graphviz


%description
This provides the runtime libraries for the abstaction library.

%package devel
Obsoletes: ldas-tools-general-devel
Group: Development/Scientific
Summary: LDAS tools abstraction toolkit development files
Requires: ldas-tools-al = 2.6.2
Requires: boost-devel
%description devel
This provides the develpement files the abstraction library.
%prep

%setup -q

%build

%configure --disable-warnings-as-errors --with-optimization=high --disable-tcl --enable-python --docdir=%{_docdir}
make V=1 %{?_smp_mflags}
make V=1 check

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;
#--------------------------------------------------------------
# Remove unwanted python init files
#--------------------------------------------------------------
find %{buildroot} -name __init__.py* -exec rm -f {} \;

%post
ldconfig
%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/libldastoolsal*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/ldastoolsal
%{_libdir}/libldastoolsal.a
%{_libdir}/libldastoolsal*.so
%{_libdir}/pkgconfig/ldastoolsal.pc
%{_docdir}/ldastoolsal

%changelog
* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.2-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.1-1
- Built for new release

* Sat Oct 22 2016 Edward Maros <ed.maros@ligo.org> - 2.5.5-1
- Built for new release

* Mon Oct 10 2016 Edward Maros <ed.maros@ligo.org> - 2.5.4-1
- Built for new release

* Mon Sep 26 2016 Edward Maros <ed.maros@ligo.org> - 2.5.3-1
- Added ability to have verbose debugging of Threads class

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <emaros@ligo.caltech.edu> - 1.19.13-1
- Initial build.
