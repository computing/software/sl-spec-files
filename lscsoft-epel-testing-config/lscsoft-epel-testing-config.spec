%define    __yumdir /etc/yum.repos.d

Name:      lscsoft-epel-testing-config
Version:   1.0
Release:   2
Summary:   Yum configuration for LSCSoft EPEL Testing Repository.
License:   GPL
Group:     LSCSoft
Source:    %{name}-%{version}.tar.xz
URL:       https://git.ligo.org/packaging/sl-spec-files/tree/master/lscsoft-epel-testingconfig
Packager:  Adam Mercer <adam.mercer@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires:  yum
Requires:  lscsoft-epel-config
BuildArch: noarch

%description
This RPM installs the required repository configuration files for
accessing the LSCSoft EPEL Testing repository.

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}/%{__yumdir}
%{__install} lscsoft-epel-testing.repo %{buildroot}/%{__yumdir}

%post
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft EPEL Testing repository is accessible"
echo

%postun
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft EPEL Testing repository has been"
echo "successfully removed from the yum configuration"
echo

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -rf ${RPM_BUILD_ROOT}
rm -rf ${RPM_BUILD_DIR}/%{name}-%{version}

%files
%defattr(0644,root,root)
%{__yumdir}/lscsoft-epel-testing.repo

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Tue Sep 29 2015 Adam Mercer <adam.mercer@ligo.org> 1.0-2
- depend on lscsoft-epel-config
* Tue Sep 29 2015 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial release
