%define name 	gds
%define version 2.17.15
%define release 2
%define daswg   /usr
%define prefix  %{daswg}
%define dmtrun 1
%if %{dmtrun}
%define _sysconfdir /etc
%define config_runflag --enable-dmt-runtime --sysconfdir=%{_sysconfdir}
%else
%define config_runflag --disable-dmt-runtime
%endif
%define withpygds 1
%if %{withpygds}
%define config_pygds --enable-python
%else
%define config_pygds --disable-python
%endif

%define root_cling %(test "`root-config --has-cling`" != "yes"; echo $?)

Name: 		%{name}
Summary: 	GDS 2.17.15
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems
Source: 	%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  gzip zlib bzip2 expat-devel libXpm-devel ldas-tools-framecpp
BuildRequires:  ldas-tools-framecpp-devel ldas-tools-al-devel libmetaio-devel
BuildRequires:  root, libcurl-devel zlib-devel hdf5-devel krb5-devel numpy
BuildRequires:  python-devel readline-devel fftw-devel cyrus-sasl-devel swig
BuildRequires:  jsoncpp-devel
AutoReqProv: 	no
Provides: 	%name
Obsoletes:	%name < %version
Prefix:		%prefix

%description
Global diagnostics software

%package core
Summary: 	GDS package Core libraries
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       ldas-tools-framecpp expat fftw hdf5 cyrus-sasl

%description core
Core libraries required by the rest of the GDS packages

%package crtools
Summary: 	Core shared objects
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}, libcurl

%description crtools
GDS control room tools

%package devel
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}
Requires:       %{name}-services = %{version}-%{release}
Requires:       expat-devel, cyrus-sasl-devel, ldas-tools-framecpp-devel

%description devel
GDS software development files.

%package services
Summary: 	GDS services
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}

%description services
GDS runtime services

%package monitors
Summary: 	DMT Monitor programs
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}, jsoncpp-devel

%description monitors
GDS DMT monitor programs

%if %withpygds
%package pygds
Summary: 	Python wrapper for gds classes
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}

%description pygds
Python wrappers of some of the most useful GDS classes.
%endif

%if %dmtrun
%package runtime
Summary: 	DMT run-time software
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-monitors = %{version}-%{release}

%description runtime
DMT run-time supervisor and services
%endif

%package web
Summary: 	DMT web services
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}, libcurl

%description web
DMT web services

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure  --prefix=%prefix --libdir=%{_libdir} \
	     --includedir=%{prefix}/include/%{name} \
	     --enable-online --enable-dtt %{config_pygds} %{config_runflag}
make V=0

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%files core
%defattr(-,root,root)
%{_bindir}/fdir
%{_bindir}/fextract
%{_bindir}/finfo
%{_bindir}/framecmp
%{_bindir}/framedump
%{_bindir}/FrameDump
%{_bindir}/Frame_Log
%{_bindir}/framelink
%{_bindir}/frcompress
%{_bindir}/FrDir
%{_bindir}/FrTest
%{_bindir}/fsettime
%{_bindir}/lsmp_mux
%{_bindir}/MergeTrends
%{_bindir}/NDS2Frame
%{_bindir}/SimList
%{_bindir}/smcreate
%{_bindir}/smdump
%{_bindir}/smkill
%{_bindir}/smlatency
%{_bindir}/smlist
%{_bindir}/smpull
%{_bindir}/smraw
%{_bindir}/smrepair
%{_bindir}/smspew
%{_bindir}/smstat
%{_bindir}/tablepgm
%{_bindir}/trendtable
%{_bindir}/trender
%{_bindir}/when
%{_libdir}/libcalcengn.so*
%{_libdir}/libdaqs.so*
%{_libdir}/libdmtsigp.so*
%{_libdir}/libframefast.so*
%{_libdir}/libframeio.so*
%{_libdir}/libframeutil.so*
%{_libdir}/libgdsbase.so*
%{_libdir}/libgdscntr.so*
%{_libdir}/libgdstrig.so*
%{_libdir}/libhtml.so*
%{_libdir}/liblmsg.so*
%{_libdir}/liblsmp.so*
%{_libdir}/liblxr.so*
%{_libdir}/libparsl.so*
%{_libdir}/libsockutil.so*
%{_libdir}/libweb.so*
%{_libdir}/libxsil.so*
%{_datadir}/%name/macros
%{_datadir}/%name/.rootrc
%{_datadir}/%name/root-version
%{_datadir}/%name/setup/root-setup
%{_prefix}/etc/gds-user-env.*
%if %{root_cling}
%{_libdir}/*.pcm
%endif

%files crtools
%defattr(-,root,root)
%{_bindir}/awggui
%{_bindir}/awgexc_run
%{_bindir}/awgstream
%{_bindir}/chndump
%{_bindir}/CY.robot
%{_bindir}/diag
%{_bindir}/diagd
%{_bindir}/diaggui
%{_bindir}/dmtviewer
%{_bindir}/fantom
%{_bindir}/foton
%{_bindir}/gdserrd
%{_bindir}/lidax
%{_bindir}/multiawgstream
%{_bindir}/tpcmd
%{_bindir}/udnls
%{_bindir}/xmlconv
%{_bindir}/xmldata
%{_bindir}/xmldir
%{_libdir}/libawg.so*
%{_libdir}/libclient.so*
%{_libdir}/libgdsalgo.so*
%{_libdir}/libdfmgui.so*
%{_libdir}/libdfm.so*
%{_libdir}/libdmtaccess.so*
%{_libdir}/libdmtview.so*
%{_libdir}/libdtt.so*
%{_libdir}/libdttview.so*
%{_libdir}/libfantom.so*
%{_libdir}/libgdsplot.so*
%{_libdir}/liblidax.so*
%{_libdir}/libligogui.so*
%{_libdir}/libmondmtsrvr.so*
%{_libdir}/libmonlmsg.so*
%{_libdir}/libSIStr.so*
%{_libdir}/libtestpoint.so*
%{_mandir}/*
%{_datadir}/%name/startup/*
%{python_sitelib}/awg*
%{python_sitelib}/foton*

%files devel
%defattr(-,root,root)
%prefix/include/*
%{_libdir}/pkgconfig/*
%{_libdir}/*.a
%{_libdir}/*.la

%files services
%{_bindir}/Alarm
%{_bindir}/AlarmCtrl
%{_bindir}/AlarmMgr
%{_bindir}/DpushM
%{_bindir}/DpushRT
%{_bindir}/DpushTease
%{_bindir}/lsmp_xmit
%{_bindir}/NameCtrl
%{_bindir}/NameServer
%{_bindir}/TrigMgr
%{_bindir}/trigRmNode
%{_bindir}/TrigRndm
%{_libdir}/libserver.so*
%{_libdir}/libframexmit.so*

%files monitors
%defattr(-,root,root)
%{_bindir}/aperture
%{_bindir}/BicoMon
%{_bindir}/BicoViewer
%{_bindir}/BitTest
%{_bindir}/blrms_monitor
%{_bindir}/burstMon
%{_bindir}/callineMon
%{_bindir}/CheckDataValid
%{_bindir}/Cumulus
%{_bindir}/DEnvCorr
%{_bindir}/dewarMon
%{_bindir}/DMTGen
%{_bindir}/dmt_wplot
%{_bindir}/dmt_wscan
%{_bindir}/dmt_wsearch
%{_bindir}/dmt_wstream
%{_bindir}/dq-module
%{_bindir}/DuoTone
%{_bindir}/dvTest
%{_bindir}/earthquake_alarm
%{_bindir}/EndTimes
%{_bindir}/eqMon
%{_bindir}/FrWrite
%{_bindir}/GainMon
%{_bindir}/glitchMon
%{_bindir}/HistCompr
%{_bindir}/InspiralMon
%{_bindir}/InspiralRange
%{_bindir}/IRIG-B
%{_bindir}/kleineWelleM
%{_bindir}/LIGOLwMon
%{_bindir}/LightMon
%{_bindir}/LineMonitor
%{_bindir}/LockLoss
%{_bindir}/mkcalibfile
%{_bindir}/MatchTrig
%{_bindir}/MultiVolt
%{_bindir}/NoiseFloorMonitor
%{_bindir}/NormTest
%{_bindir}/OmegaMon
%{_bindir}/PCalMon
%{_bindir}/PhotonCal
%{_bindir}/PlaneMon
%{_bindir}/PSLmon
%{_bindir}/PulsarMon
%{_bindir}/RayleighMonitor
%{_bindir}/seg_calc
%{_bindir}/SegGener
%{_bindir}/SenseMonitor
%{_bindir}/ShapeMon
%{_bindir}/SixtyHertzMon
%{_bindir}/SpectrumArchiver
%{_bindir}/SpectrumFold
%{_bindir}/Station
%{_bindir}/StochMon
%{_bindir}/StrainbandsMon
%{_bindir}/suspensionMon
%{_bindir}/TimeMon
%{_bindir}/TrigDsply
%{_bindir}/TrigSpec
%{_bindir}/WaveMon
%{_libdir}/libdqplugins.so*
%{_libdir}/libezcalib.so*
%{_libdir}/libgdsevent.so*
%{_libdir}/libgenerator.so*
%{_libdir}/liblscemul.so*
%{_libdir}/libmonitor.so*
%{_libdir}/libosc.so*
%{_libdir}/libtclient.so*
%{_libdir}/libwpipe.so*
%{_datadir}/%name/man

%if %withpygds
%files pygds
%python_sitearch/%name/*
%endif

%if %dmtrun
%defattr(-,root,root)
%files runtime
%defattr(-,root,root)
%{_bindir}/AlarmOnHTML
%{_bindir}/confCompare
%{_bindir}/cpName
%{_bindir}/dmtstatus
%{_bindir}/dmt
%{_bindir}/dmt-mon_disable
%{_bindir}/dmt-mon_enable
%{_bindir}/dmt-mon_info
%{_bindir}/dmt-mon_restart
%{_bindir}/dmt-mon_status
%{_bindir}/dmt-scriplets
%{_bindir}/findProc
%{_bindir}/makeIndex
%{_bindir}/MonTrend
%{_bindir}/mkTrendDirs
%{_bindir}/NameBackup
%{_bindir}/no_insert
%{_bindir}/procmgt
%{_bindir}/procntrl
%{_bindir}/procsetup
%{_bindir}/prune.pl
%{_bindir}/PSD_PlotMaker
%{_bindir}/restart_history.sh
%{_bindir}/SegData
%{_bindir}/SegDirectory
%{_bindir}/SegEpics
%{_bindir}/SPI.pl
%{_bindir}/TrendAvg
%{_bindir}/USAGE.tcsh
%{_bindir}/waitProc
%{_bindir}/WatchDawg
%{_bindir}/WEEDER.tcsh
%{_datadir}/%name/Alarms
%{_datadir}/%name/procmgt
%{_datadir}/%name/PSD_PlotMaker
%{_datadir}/%name/Spi
%{_datadir}/%name/setup/addpath
%{_datadir}/%name/setup/addpathb
%{_datadir}/%name/setup/dmtsetup.csh
%{_datadir}/%name/setup/dmtsetup.sh
%{_datadir}/%name/setup/rempath
%{_datadir}/%name/setup/rempathb
%{_sysconfdir}/sysconfig/default-dmt-procmgt
%{_sysconfdir}/init.d/dmt-procmgt
%endif

%files web
%defattr(-,root,root)
%{_bindir}/GraphIt
%{_bindir}/webview
%{_bindir}/webxmledit
