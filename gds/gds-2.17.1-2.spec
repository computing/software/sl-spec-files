%define name 	gds
%define version 2.17.1
%define release 2
%define daswg   /usr
%define prefix  %{daswg}
%define dmtrun 1
%if %{dmtrun}
%define _sysconfdir /etc
%define config_runflag --enable-dmt-runtime --sysconfdir=%{_sysconfdir}
%else
%define config_runflag --disable-dmt-runtime
%endif

Name: 		%{name}
Summary: 	GDS 2.17.1
Version: 	%{version}
Release: 	%{release}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems
Source: 	%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  gzip zlib bzip2 expat-devel libXpm-devel ldas-tools-framecpp
BuildRequires:  ldas-tools-framecpp-devel ldas-tools-al-devel libmetaio-devel
BuildRequires:  root, libcurl-devel zlib-devel hdf5-devel krb5-devel nawk
BuildRequires:  readline-devel fftw-devel cyrus-sasl-devel swig python-devel
BuildRequires:  numpy
AutoReqProv: 	no
Provides: 	%name
Obsoletes:	%name < %version
Prefix:		%prefix

%description
Global diagnostics software

%package core
Summary: 	GDS package Core libraries
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       ldas-tools-framecpp expat fftw hdf5 cyrus-sasl libmetaio

%description core
Core libraries required by the rest of the GDS packages

%package crtools
Summary: 	Core shared objects
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}, libcurl

%description crtools
GDS control room tools

%package devel
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}
Requires:       %{name}-services = %{version}-%{release}
Requires:       expat-devel, cyrus-sasl-devel, ldas-tools-framecpp-devel

%description devel
GDS software development files.

%package services
Summary: 	GDS services
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}

%description services
GDS runtime services

%package monitors
Summary: 	DMT Monitor programs
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}

%description monitors
GDS DMT monitor programs

%if 1
%package pygds
Summary: 	Python wrapper for gds classes
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}

%description pygds
Python wrappers of some of the most useful GDS classes.
%endif

%if %dmtrun
%package runtime
Summary: 	DMT run-time software
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-monitors = %{version}-%{release}

%description runtime
DMT run-time supervisor and services
%endif

%package web
Summary: 	DMT web services
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-core = %{version}-%{release}, libcurl, plotutils, nawk

%description web
DMT web services

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure  --prefix=%prefix --libdir=%{_libdir} \
	     --includedir=%{prefix}/include/%{name} --with-simd=native \
	     --enable-online --enable-dtt --enable-python %{config_runflag}
make V=0

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%files core
%defattr(-,root,root)
%{_bindir}/fdir
%{_bindir}/fextract
%{_bindir}/finfo
%{_bindir}/framecmp
%{_bindir}/framedump
%{_bindir}/FrameDump
%{_bindir}/Frame_Log
%{_bindir}/framelink
%{_bindir}/frcompress
%{_bindir}/FrDir
%{_bindir}/FrTest
%{_bindir}/fsettime
%{_bindir}/MergeTrends
%{_bindir}/NDS2Frame
%{_bindir}/prchans
%{_bindir}/prdata
%{_bindir}/prnames
%{_bindir}/prndsvsn
%{_bindir}/seg_calc
%{_bindir}/SimList
%{_bindir}/smcreate
%{_bindir}/smdump
%{_bindir}/smkill
%{_bindir}/smlatency
%{_bindir}/smlist
%{_bindir}/smraw
%{_bindir}/smrepair
%{_bindir}/smspew
%{_bindir}/smstat
%{_bindir}/tablepgm
%{_bindir}/trendtable
%{_bindir}/when
%{_libdir}/libcalcengn.so*
%{_libdir}/libdaqs.so*
%{_libdir}/libdmtsigp.so*
%{_libdir}/libframefast.so*
%{_libdir}/libframeio.so*
%{_libdir}/libframeutil.so*
%{_libdir}/libgdsbase.so*
%{_libdir}/libgdscntr.so*
%{_libdir}/libgdstrig.so*
%{_libdir}/libhtml.so*
%{_libdir}/liblmsg.so*
%{_libdir}/liblsmp.so*
%{_libdir}/liblxr.so*
%{_libdir}/libparsl.so*
%{_libdir}/libsockutil.so*
%{_libdir}/libweb.so*
%{_libdir}/libxsil.so*
%{_datadir}/%name/macros
%{_datadir}/%name/.rootrc
%{_datadir}/%name/root-version
%{_prefix}/etc/gds-user-env.*

%files crtools
%defattr(-,root,root)
%{_bindir}/awggui
%{_bindir}/chndump
%{_bindir}/CY.robot
%{_bindir}/diag
%{_bindir}/diagd
%{_bindir}/diaggui
%{_bindir}/dmtviewer
%{_bindir}/fantom
%{_bindir}/foton
%{_bindir}/gdserrd
%{_bindir}/lidax
%{_bindir}/tpcmd
%{_bindir}/udnls
%{_bindir}/xmlconv
%{_bindir}/xmldata
%{_bindir}/xmldir
%{_libdir}/libawg.so*
%{_libdir}/libclient.so*
%{_libdir}/libgdsalgo.so*
%{_libdir}/libdfmgui.so*
%{_libdir}/libdfm.so*
%{_libdir}/libdmtaccess.so*
%{_libdir}/libdmtview.so*
%{_libdir}/libdtt.so*
%{_libdir}/libdttview.so*
%{_libdir}/libfantom.so*
%{_libdir}/libgdsplot.so*
%{_libdir}/liblidax.so*
%{_libdir}/libligogui.so*
%{_libdir}/libmondmtsrvr.so*
%{_libdir}/libmonlmsg.so*
%{_libdir}/libtestpoint.so*
%{_mandir}/*
%{_datadir}/%name/startup/*
%{_datadir}/awggui_changes
%{_datadir}/diaggui_changes
%{_datadir}/foton_changes
%{python_sitelib}/awg*
%{python_sitelib}/foton*

%files devel
%defattr(-,root,root)
%prefix/include/*
%{_libdir}/pkgconfig/*
%{_libdir}/*.a
%{_libdir}/*.la

%files services
%{_bindir}/Alarm
%{_bindir}/AlarmCtrl
%{_bindir}/AlarmMgr
%{_bindir}/DpushM
%{_bindir}/DpushRT
%{_bindir}/lsmp_xmit
%{_bindir}/NameCtrl
%{_bindir}/NameServer
%{_bindir}/TrigMgr
%{_bindir}/trigRmNode
%{_bindir}/TrigRndm
%{_libdir}/libserver.so*
%{_libdir}/libframexmit.so*

%files monitors
%defattr(-,root,root)
%{_bindir}/BicoMon
%{_bindir}/BicoViewer
%{_bindir}/BitTest
%{_bindir}/blrms_monitor
%{_bindir}/burstMon
%{_bindir}/callineMon
%{_bindir}/CheckDataValid
%{_bindir}/Cumulus
%{_bindir}/DEnvCorr
%{_bindir}/dewarMon
%{_bindir}/DMTGen
%{_bindir}/dmt_wplot
%{_bindir}/dmt_wscan
%{_bindir}/dmt_wsearch
%{_bindir}/dmt_wstream
%{_bindir}/DuoTone
%{_bindir}/dvTest
%{_bindir}/earthquake_alarm
%{_bindir}/eqMon
%{_bindir}/FrWrite
%{_bindir}/GainMon
%{_bindir}/glitchMon
%{_bindir}/HistCompr
%{_bindir}/InspiralMon
%{_bindir}/InspiralRange
%{_bindir}/IRIG-B
%{_bindir}/kleineWelleM
%{_bindir}/LIGOLwMon
%{_bindir}/LightMon
%{_bindir}/LineMonitor
%{_bindir}/LockLoss
%{_bindir}/mkcalibfile
%{_bindir}/MatchTrig
%{_bindir}/MultiVolt
%{_bindir}/NoiseFloorMonitor
%{_bindir}/NormTest
%{_bindir}/OmegaMon
%{_bindir}/PCalMon
%{_bindir}/PhotonCal
%{_bindir}/PlaneMon
%{_bindir}/PSLmon
%{_bindir}/PulsarMon
%{_bindir}/RayleighMonitor
%{_bindir}/SegGener
%{_bindir}/SenseMonitor
%{_bindir}/ShapeMon
%{_bindir}/SixtyHertzMon
%{_bindir}/SpectrumArchiver
%{_bindir}/SpectrumFold
%{_bindir}/Station
%{_bindir}/StochMon
%{_bindir}/StrainbandsMon
%{_bindir}/suspensionMon
%{_bindir}/TimeMon
%{_bindir}/TrigDsply
%{_bindir}/TrigSpec
%{_bindir}/WaveMon
%{_libdir}/libezcalib.so*
%{_libdir}/libgdsevent.so*
%{_libdir}/libgenerator.so*
%{_libdir}/liblscemul.so*
%{_libdir}/libmonitor.so*
%{_libdir}/libosc.so*
%{_libdir}/libtclient.so*
%{_libdir}/libwpipe.so*
%{_datadir}/%name/man

%if 1
%files pygds
%python_sitearch/%name/*
%endif

%if %dmtrun
%defattr(-,root,root)
%files runtime
%defattr(-,root,root)
%{_bindir}/AlarmOnHTML
%{_bindir}/confCompare
%{_bindir}/cpName
%{_bindir}/dmtstatus
%{_bindir}/dmt-mon_disable
%{_bindir}/dmt-mon_enable
%{_bindir}/dmt-mon_restart
%{_bindir}/dmt-mon_status
%{_bindir}/dmt-scriplets
%{_bindir}/findProc
%{_bindir}/makeIndex
%{_bindir}/MonTrend
%{_bindir}/mkTrendDirs
%{_bindir}/NameBackup
%{_bindir}/no_insert
%{_bindir}/procmgt
%{_bindir}/procntrl
%{_bindir}/procsetup
%{_bindir}/prune.pl
%{_bindir}/PSD_PlotMaker
%{_bindir}/SegData
%{_bindir}/SegDirectory
%{_bindir}/SegEpics
%{_bindir}/SPI.pl
%{_bindir}/TrendAvg
%{_bindir}/USAGE.tcsh
%{_bindir}/waitProc
%{_bindir}/WatchDawg
%{_bindir}/WEEDER.tcsh
%{_datadir}/%name/Alarms
%{_datadir}/%name/procmgt
%{_datadir}/%name/PSD_PlotMaker
%{_datadir}/%name/Spi
%{_datadir}/%name/setup/addpath
%{_datadir}/%name/setup/addpathb
%{_datadir}/%name/setup/dmtsetup.csh
%{_datadir}/%name/setup/dmtsetup.sh
%{_datadir}/%name/setup/rempath
%{_datadir}/%name/setup/rempathb
%{_datadir}/%name/setup/root-setup
%{_sysconfdir}/sysconfig/default-dmt-procmgt
%{_sysconfdir}/init.d/dmt-procmgt
%endif

%files web
%defattr(-,root,root)
%{_bindir}/GraphIt
%{_bindir}/webview
%{_bindir}/webxmledit
