%define gstreamername gstreamer1

Name: gstlal-ugly
Version: 1.5.1
Release: 2%{?dist}
Summary: GSTLAL Experimental Supplements
License: GPL
Group: LSC Software/Data Analysis
Requires: gstlal >= 1.3.0
Requires: python >= 2.7
Requires: glue >= 1.58.0
Requires: glue-segments >= 1.58.0
Requires: gobject-introspection >= 1.30.0
Requires: fftw >= 3
Requires: %{gstreamername} >= 1.2.4
Requires: %{gstreamername}-plugins-base >= 1.2.4
Requires: %{gstreamername}-plugins-good >= 1.2.4
Requires: %{gstreamername}-plugins-bad-free
Requires: numpy
Requires: scipy
Requires: lal >= 6.18.0
Requires: lalmetaio >= 1.3.1
Requires: gsl
Requires: ldas-tools-framecpp >= 2.5.8
Requires: gds-lowlatency >= 2.18.0
Requires: nds2-client >= 0.11.5
Requires: orc >= 0.4.16
Requires: python-%{gstreamername}
BuildRequires: doxygen  >= 1.8.3
BuildRequires: graphviz
BuildRequires: gstlal-devel >= 1.3.0
BuildRequires: python-devel >= 2.7
BuildRequires: fftw-devel >= 3
BuildRequires: gobject-introspection-devel >= 1.30.0
BuildRequires: %{gstreamername}-devel >= 1.2.4
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.2.4
BuildRequires: lal-devel >= 6.18.0
BuildRequires: lalmetaio-devel >= 1.3.1
BuildRequires: gsl-devel
BuildRequires: gtk-doc >= 1.11
BuildRequires: ldas-tools-framecpp-devel >= 2.5.8
BuildRequires: gds-devel >= 2.18.0
BuildRequires: gds-lowlatency >= 2.18.0
BuildRequires: nds2-client-devel >= 0.11.5
BuildRequires: nds2-client-headers >= 0.11.5
BuildRequires: orc >= 0.4.16
Source: gstlal-ugly-%{version}.tar.gz
URL: https://wiki.ligo.org/DASWG/GstLAL
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains the plugins and shared libraries required to run
gstlal-based applications.


%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version} gstlal-devel >= 1.3.0 python-devel >= 2.7 fftw-devel >= 3 %{gstreamername}-devel >= 1.2.4 %{gstreamername}-plugins-base-devel >= 1.2.4 lal-devel >= 6.18.0 lalmetaio-devel >= 1.3.1 gsl-devel nds2-client-headers >= 0.11.5
%description devel
This package contains the files needed for building gstlal-ugly based
plugins and programs.


%prep
%setup -q -n %{name}-%{version}


%build
%configure --enable-gtk-doc
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gstlal/*
# there's no documentation
#%{_docdir}/gstlal-ugly-*
#%{_libdir}/*.so.*
%{_libdir}/gstreamer-1.0/*.so
#%{_libdir}/gstreamer-1.0/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
#%{_includedir}/gstlal-ugly
#%{_libdir}/*.a
#%{_libdir}/*.so
%{_libdir}/gstreamer-1.0/*.a
#%{_libdir}/pkgconfig/*
