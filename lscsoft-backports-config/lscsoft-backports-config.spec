%define    __yumdir /etc/yum.repos.d

Name:      lscsoft-backports-config
Version:   1.3
Release:   1%{?dist}
Summary:   Yum configuration for LSCSoft Backports repository
License:   GPL
Group:     LSCSoft
Source:    %{name}-%{version}.tar.xz
URL:       https://git.ligo.org/packaging/sl-spec-files/tree/master/lscsoft-backports-config
Packager:  Adam Mercer <adam.mercer@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires:  yum
BuildArch: noarch

%description
This RPM installed the required repository configuration files for
accessing the LSCSoft Backports repository.

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}/%{__yumdir}
%{__install} lscsoft-backports.repo %{buildroot}/%{__yumdir}

%post
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft Backports repository is accessible"
echo

%postun
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft Backports repository has been"
echo "successfully removed from the yum configuration"
echo

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -rf ${RPM_BUILD_ROOT}
rm -rf ${RPM_BUILD_DIR}/%{name}-%{version}

%files
%defattr(0644,root,root)
%{__yumdir}/lscsoft-backports.repo

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Tue Jan 09 2018 Adam Mercer <adam.mercer@ligo.org> 1.3-1
- set priority to 97

* Mon Mar 21 2016 Adam Mercer <adam.mercer@ligo.org> 1.2-1
- use $releasever for version, allows easy support for both SL6 and SL7

* Thu Mar 05 2015 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- fix typo in baseurl

* Thu Mar 05 2015 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial release
