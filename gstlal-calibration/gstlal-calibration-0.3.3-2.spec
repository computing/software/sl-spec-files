%define gstreamername gstreamer_lscsoft

Name: gstlal-calibration
Version: 0.3.3
Release: 2.lscsoft
Summary: GSTLAL Calibration
License: GPL
Group: LSC Software/Data Analysis
Requires: gstlal >= 0.7.0 python >= 2.6 glue >= 1.46 glue-segments >= 1.46 python-pylal >= 0.5.0 %{gstreamername} >= 0.10.32 %{gstreamername}-plugins-base >= 0.10.32 %{gstreamername}-plugins-good >= 0.10.27 gstreamer_lscsoft-python >= 0.10.21 pygobject2 numpy scipy lal >= 6.12.0 lalmetaio >= 1.2.0
BuildRequires: gstlal-devel >= 0.7.0 python-devel >= 2.6 fftw-devel >= 3 %{gstreamername}-devel >= 0.10.32 %{gstreamername}-plugins-base-devel >= 0.10.32 pygobject2-devel lal-devel >= 6.12.0 lalmetaio-devel >= 1.2.0
Conflicts: gstlal-ugly < 0.6.0
Source: gstlal-calibration-%{version}.tar.gz
URL: https://www.lsc-group.phys.uwm.edu/daswg/projects/gstlal.html
Packager: Madeline Wade <madeline.wade@gravity.phys.uwm.edu>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package contains the plugins and shared libraries required to run the gstlal calibration software.


%prep
%setup -q -n %{name}-%{version}


%build
. /opt/lscsoft/gst/gstenvironment.sh
%configure
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/gstreamer-0.10/python/*
%{_libdir}/gstreamer-0.10/libgstlalcalibration.a
%{_libdir}/gstreamer-0.10/libgstlalcalibration.so
