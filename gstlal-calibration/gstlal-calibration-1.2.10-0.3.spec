%define gstreamername gstreamer1

Name: gstlal-calibration
Version: 1.2.10
Release: 0.3%{?dist}
Summary: GSTLAL Calibration
License: GPL
Group: LSC Software/Data Analysis

Requires: gstlal >= 1.0.0
Requires: gstlal-ugly >= 1.0.0
Requires: python >= 2.7
Requires: python2-ligo-segments >= 1.1.0
Requires: %{gstreamername} >= 1.14.1
Requires: %{gstreamername}-plugins-base >= 1.14.1
Requires: %{gstreamername}-plugins-good >= 1.14.1
Requires: python-%{gstreamername}
Requires: numpy
Requires: scipy
Requires: lal >= 6.15.2
Requires: lalmetaio >= 1.2.6

BuildRequires: gstlal-devel >= 1.0.0
BuildRequires: python-devel >= 2.7
BuildRequires: fftw-devel >= 3
BuildRequires: %{gstreamername}-devel >= 1.14.1
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.14.1
BuildRequires: lal-devel >= 6.15.2
BuildRequires: lalmetaio-devel >= 1.2.6

Conflicts: gstlal-ugly < 0.6.0
Source: gstlal-calibration-%{version}.tar.gz
URL: https://www.lsc-group.phys.uwm.edu/daswg/projects/gstlal.html
Packager: Madeline Wade <madeline.wade@gravity.phys.uwm.edu>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package contains the plugins and shared libraries required to run the gstlal calibration software.


%prep
%setup -q -n %{name}-%{version}


%build
%configure
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/gstreamer-*/lib*.a
%{_libdir}/gstreamer-*/lib*.so
%{_prefix}/%{_lib}/python*/site-packages/gstlal
