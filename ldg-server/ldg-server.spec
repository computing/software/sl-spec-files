Name: ldg-server
Version: 5.2.1
Release: 1
Group: LDG
License: BSD / Globus
Summary: LDG Server Packages
Packager: Adam Mercer <adam.mercer@ligo.org>
Requires: ldg-core, ldg-client, globus-gatekeeper
Requires: globus-gridftp-server-progs, ldg-gridftp-server-config
Requires: vdt-gsi-openssh-server, vdt-mechglue-ncsa
Requires: ligo-grid-mapfile-manager
BuildArch: noarch

%description
This meta-package will pull in all necessary packages to provide the
administrator with a complete server configuration. Please bear in mind
that you need to look into the SSHD and the GRAM configurations - you
have been warned!

%install

%files

%changelog
*
- use multiple Requires lines
- add changelog

* Tue Mar 26 2013 Adam Mercer <adam.mercer@ligo.org> 5.2.1-1
- add dependency on ligo-grid-mapfilr-manager

* Fri Jan 13 2012 Adam Mercer <adam.mercer@ligo.org> 5.2-2
- add dependency on ldg-gridftp-server-config

* Wed Nov 02 2011 Adam Mercer <adam.mercer@ligo.org> 5.2-1
- update dependencies for SL6

* Wed Apr 27 2011 Adam Mercer <adam.mercer@ligo.org> 5.1-1
- initial version
