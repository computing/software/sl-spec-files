# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define tarbasename ldas-tools-filters
%define _docdir     %{_datadir}/doc/%{tarbasename}-%{version}

%if %{?cmake3:1}%{!?cmake3:0}
%define __cmake %__cmake3
%define cmake %cmake3
%define ctest ctest3
%else
%define ctest ctest
%endif

Summary: Filters library used by ldas-tools
Name: ldas-tools-filters
Version: 2.6.3
Release: 2%{?dist}
License: GPLv2+
URL: http://www.ligo.caltech.edu
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: http://software.ligo.org/lscsoft/source/ldas-tools-filters-%{version}.tar.gz
Requires: ldas-tools-al >= 2.6.2
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: gawk
BuildRequires: make
BuildRequires: rpm-build
%if 0%{?rhl} <= 7 || 0%{?sl7} <= 7
BuildRequires: cmake3 >= 3.6
BuildRequires: cmake
%else
BuildRequires: cmake >= 3.6
%endif
Buildrequires: doxygen
Buildrequires: pkgconfig
Buildrequires: ldas-tools-cmake >= 1.0.5
Buildrequires: ldas-tools-al-devel >= 2.6.2

%description
This provides the runtime libraries for the filters library.

%package devel
Group: Development/Scientific
Summary: LDAS tools libfilters toolkit development files
Requires: ldas-tools-al-devel >= 2.6.2
Requires: ldas-tools-filters = %{version}
%description devel
This provides the develpement files the filters library.
%prep

%setup -c -T -D -a 0 -n %{name}-%{version}

%build
%cmake \
    %{python_build_opts} \
    %{python3_build_opts} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    %{tarbasename}-%{version}

make VERBOSE=1 %{?_smp_mflags}


%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make VERBOSE=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;
rm -f %{buildroot}%{python_sitearch}/LDAStools/_*.a
rm -f %{buildroot}%{python_sitearch}/LDAStools/libdiskcacheAPI_python.a

%check
%ctest -V %{?_smp_mflags}

%post
ldconfig
%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/libfilters*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/filters
%{_docdir}
%{_libdir}/libfilters*.*a
%{_libdir}/libfilters*.so
%{_libdir}/pkgconfig/ldas-tools-filters.pc

%changelog
* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.3-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.2-1
- Built for new release

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <ed.maros@ligo.org> - 1.19.13-1
- Initial build.
