%global with_python3 0
%global upname seaborn

Name: python-%{upname}
Version: 0.5.1
Release: 6.lscsoft%{?dist}
Summary: Statistical data visualization in Python
# The source in pypi contains only the package code
# no readme, no license
# https://github.com/mwaskom/seaborn/issues/378
License: BSD

URL: http://stanford.edu/~mwaskom/software/seaborn/
Source0: https://pypi.python.org/packages/source/s/seaborn/seaborn-%{version}.tar.gz
# License from upstream repository
Source1: https://raw.githubusercontent.com/mwaskom/seaborn/master/LICENSE
# Use system python-six
Patch0: seaborn-0.5.0-six.patch
# Use system python-husl
Patch1: seaborn-0.5.0-husl.patch
# Remove expty 'seaborn.external' subpackage
Patch2: seaborn-0.5.0-external.patch
BuildArch: noarch

BuildRequires: python2-devel python-setuptools
BuildRequires: numpy scipy python-matplotlib python-pandas
BuildRequires: python-six python-husl
BuildRequires: python-nose
Requires: numpy scipy python-matplotlib python-pandas
Requires: python-six python-husl
Requires: python-nose

%description
Seaborn is a library for making attractive and informative statistical 
graphics in Python. It is built on top of matplotlib and tightly integrated 
with the PyData stack, including support for numpy and pandas data structures 
and statistical routines from scipy and statsmodels.

%if 0%{?with_python3}
%package -n python3-%{upname}
Summary: Statistical data visualization in Python
BuildRequires: python3-devel python3-setuptools
BuildRequires: python3-numpy python3-scipy
BuildRequires: python3-matplotlib python3-pandas
BuildRequires: python3-six python3-husl
BuildRequires: python3-nose
Requires: python3-numpy python3-scipy 
Requires: python3-matplotlib python3-pandas
Requires: python3-six python3-husl
Requires: python3-nose

%description -n python3-%{upname}
Seaborn is a library for making attractive and informative statistical 
graphics in Python. It is built on top of matplotlib and tightly integrated 
with the PyData stack, including support for numpy and pandas data structures 
and statistical routines from scipy and statsmodels.

%endif # with_python3

%prep
%setup -q -n %{upname}-%{version}
# Remove bundled libraries
%patch0 -p1
%patch1 -p1
%patch2 -p1
rm -rf seaborn/external/
cp %{SOURCE1} .

find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python2}|'

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'
%endif # with_python3

%build
%{__python2} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif # with_python3

%install
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif # with_python3

%{__python2} setup.py install --skip-build --root %{buildroot}
 
#%check
# Fake matplotlibrc for testing
#mkdir matplotlib
#touch matplotlib/matplotlibrc
#export XDG_CONFIG_HOME=`pwd`
#pushd %{buildroot}/%{python2_sitelib}
#nosetests-%{python2_version} -x seaborn
#popd

#%if 0%{?with_python3}
#pushd %{buildroot}/%{python3_sitelib}
#nosetests-%{python3_version} -x seaborn
#popd

#%endif # with_python3

%files
%doc LICENSE
%{python2_sitelib}/seaborn
%{python2_sitelib}/seaborn-%{version}-py%{python2_version}.egg-info

%if 0%{?with_python3}
%doc LICENSE
%files -n python3-%{upname}
%{python3_sitelib}/seaborn
%{python3_sitelib}/seaborn-%{version}-py%{python3_version}.egg-info
%endif # with_python3

%changelog
* Thu Dec 15 2016 Adam Mercer <adam.mercer@ligo.org> - 0.5.1-6.lscsoft
- disable python3 packages
- comment out running of broken testsuite

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.1-6
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Thu Nov 27 2014 Sergio Pascual <sergiopr at fedoraproject.com> - 0.5.1-4
- Add source of LICENSE from upstream, distribution of LICENSE is required

* Thu Nov 27 2014 Sergio Pascual <sergiopr at fedoraproject.com> - 0.5.1-3
- More comments

* Wed Nov 26 2014 Sergio Pascual <sergiopr at fedoraproject.com> - 0.5.1-2
- Added BRs: six and husl

* Mon Nov 17 2014 Sergio Pascual <sergiopr at fedoraproject.com> - 0.5.1-1
- Initial spec

