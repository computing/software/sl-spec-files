#========================================================================
#    NDS2 server rpm installation
#========================================================================
%define name 	nds2-server
%define version 0.8.1
%define release 4
%define _prefix  /usr
%define _sysconfdir /etc

#========================================================================
# Main spec file
#========================================================================
Name: 		%{name}
Summary: 	NDS2 Server
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LSC Data Services
Source: 	%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		https://www.lsc-group.phys.uwm.edu/daswg/wiki/NetworkDataServer2
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  ldas-tools-framecpp-devel, zlib-devel, cyrus-sasl-devel
BuildRequires:  gds-devel, gds-lowlatency, boost-devel
Summary: 	NDS2 server software
Provides: 	%name
Obsoletes:	%name < %version
Prefix:		%_prefix

Requires:	ldas-tools-framecpp
%description
The NDS2 server serves time-series data to authenticated LSC data analysis projects.

%package devel
Group: Development/Scientific
Summary: NDS2 headers
Requires:       %{name} = %{version}-%{release}, ldas-tools-framecpp-devel
%description devel
This provides a separate package to install the headers

%package init
Group: Development/Scientific
Summary: NDS2 server init files
Requires:       %{name} = %{version}-%{release}
%description init
This package contains the system init startup scripts for an nds2 server

%package util
Group: Development/Scientific
Summary: NDS2 utilties
Requires:       %{name} = %{version}-%{release}, python-ldap
%description util
This provides a separate package to install configuration utilties.

#----------------------------------------------
# Get onto the fun of building the NDS2 server
#----------------------------------------------
%prep
%setup

%build
PKG_CONFIG_PATH=/usr/%{_lib}/pkgconfig
export PKG_CONFIG_PATH
%configure --includedir=%{_includedir}/%{name}
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root)
%_bindir/nds2
%_libdir/lib*.so*

%files util
%_bindir/ChanSearch
%_bindir/buildChannelList
%_bindir/build_source_list
%_bindir/channel_merge
%_bindir/build_channel_history
%_bindir/nds2_nightly
%_bindir/nds2_sourcery
%_bindir/nds2_patch_history
%_bindir/nds2_prune_files
%_bindir/nds2_ldap_group.py

%files init
%config(noreplace) %{_sysconfdir}/sysconfig/nds2-server
%config(noreplace) %{_sysconfdir}/nds2/*
%{_sysconfdir}/init.d/nds2
%{_datadir}/nds2/*

%files devel
%_includedir/*
%_libdir/lib*.a
%_libdir/lib*.la
%_libdir/pkgconfig/nds2-server.pc
