Name: gst-plugins-math
Version: 1.0.1
Release: 1%{?dist}
Summary: Mathematical operations plugins for GStreamer
License: GPLv2+
Group: LSC Software/Data Analysis
Requires: gstreamer1 gstreamer1-plugins-base
BuildRequires: autoconf automake libtool
BuildRequires: gstreamer1-devel gstreamer1-plugins-base-devel
Source: https://github.com/lpsinger/${name}/archive/v%{version}/%{name}-%{version}.tar.gz
URL: https://github.com/lpsinger/${name}
Packager: Leo Singer <leo.singer@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root

%description
Mathematical operations plugins for GStreamer

%prep
%setup -q

%build
autoreconf -vi
%configure
%{__make}

%install
%makeinstall
find $RPM_BUILD_ROOT -name '*.la' -delete

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_libdir}/gstreamer-1.0/*.so

%changelog
* Fri Aug 26 2016 Leo Singer <leo.singer@ligo.org> 1.0.1-1

- New package
