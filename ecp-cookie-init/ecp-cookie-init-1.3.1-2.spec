Summary: Utility for obtaining short-lived cookies for accessing Shibbolized SPs from command-line tools (e.g., curl or git)
Name: ecp-cookie-init
Version: 1.3.1
Release: 2%{?dist}
Source0: ecp-cookie-init-%{version}.tar.gz
License: Unknown
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Url: https://wiki.ligo.org/AuthProject
Requires: bash, python, curl >= 7.19.7-26, libxslt >= 1.1.26-2, osg-ca-certs >= 1.40

%description
%{summary}

%prep
%setup -q 

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/bin
install --mode=0755 ecp-cookie-init $RPM_BUILD_ROOT/usr/bin/ecp-cookie-init

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0755,root,root,-)
/usr/bin/ecp-cookie-init

%changelog
* Mon Jul 20 2015 Paul Hopkins <paul.hopkins@ligo.org> - 1.3.1-1
- Added automatic failover for LIGO IdP servers
* Thu Apr 03 2013 Peter Couvares <peter.couvares@ligo.org> - 1.1.0-1
- Fixed for MacOS.
- Added LIGO Guest and Cardiff University IdP support.
- Fixed typo in error message.

* Thu Mar 14 2013 Peter Couvares <peter.couvares@ligo.org> - 1.0.0-1
- Initial version.
