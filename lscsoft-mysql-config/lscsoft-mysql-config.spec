%define    __yumdir /etc/yum.repos.d

Name:      lscsoft-mysql-config
Version:   1.0
Release:   1
Summary:   Yum configuration for LSCSoft MySQL Repository.
License:   GPL
Group:     LSCSoft
Source:    %{name}-%{version}.tar.xz
URL:       https://git.ligo.org/packaging/sl-spec-files/tree/master/lscsoft-mysql-config
Packager:  Adam Mercer <adam.mercer@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires:  yum
BuildArch: noarch

%description
This RPM installs the required repository configuration files for
accessing the LSCSoft MySQL repository.

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}/%{__yumdir}
%{__install} lscsoft-mysql.repo %{buildroot}/%{__yumdir}

%post
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft MySQL repository is accessible"
echo

%postun
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft MySQL repository has been"
echo "successfully removed from the yum configuration"
echo

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -rf ${RPM_BUILD_ROOT}
rm -rf ${RPM_BUILD_DIR}/%{name}-%{version}

%files
%defattr(0644,root,root)
%{__yumdir}/lscsoft-mysql.repo

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Wed Sep 30 2015 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial release
