%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:               python-pyRXP
Version:            1.13
Release:            2%{?dist}
Epoch:              1
Summary:            The fastest validating XML parser available for Python, and quite possibly anywhere
Group:              Development/Libraries/Python
License:            GPL
URL:                http://www.reportlab.com/xml/pyrxp.html
Source0:            pyRXP-%{version}.tar.gz
BuildRoot:          %{_tmppath}/%{name}-buildroot
Prefix:             %{_prefix}

BuildRequires:  python-devel

%description
RXP is a very fast validating XML parser written by Richard Tobin of the
University of Edinburgh. It complies fully with the W3C test suites.

pyRXP is a wrapper around this which constructs a lightweight in-memory "tuple
tree" in a single call. This structure is the lightest one we could define in
Python, and it is constructed entirely in C code, resulting in unprecedented
speed.

%prep
%setup -q -n pyRXP-%{version}

%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install \
        --skip-build \
        --root %{buildroot} --record=/tmp/pyrxp

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README docs examples rxp/COPYING rxp/Manual
%{python_sitearch}/pyRXP.so
%{python_sitearch}/pyRXPU.so
%{python_sitearch}/pyRXP-1.13-py*.egg-info

%changelog
* Tue Aug 23 2011 Xavier Amador <xavier.amador@gravity.phys.uwm.edu> 1.13.2
- Built Scientific Linux6.1 version for LIGO/VIRGO Scientific Collaboration.

* Tue Jun 17 2008 - Duncan Brown <dabrown@phy.syr.edy> - 1.13-1
- Build for CentOS 5

* Tue Nov 04 2003 - James Oakley <jfunk@funktronics.ca> - 0.9-2
- Build for SUSE 9.0

* Thu Aug 14 2003 - James Oakley <jfunk@funktronics.ca> - 0.9-1
- Initial release
