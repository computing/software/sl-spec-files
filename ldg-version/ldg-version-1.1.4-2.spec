Name: ldg-version
Version: 1.1.4
Release: 2%{?dist}
Summary: LIGO Data Grid version script
License: GPL
Group:  LSCSOFT
Source: %{name}-%{version}.tar.gz
URL: http://www.lsc-group.phys.uwm.edu/repositories.html
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires: ecp-cookie-init
Requires: globus-common-progs
Requires: ldg-core
Requires: ldg-upgrade
Requires: libcurl-devel
Requires: libxslt
Requires: ligo-proxy-utils
Requires: myproxy
Requires: osg-ca-certs
Requires: python
Requires: /usr/bin/gsissh
Obsoletes: ldg-server-version ldg-client-version
BuildArch: noarch

%description
Script to report version of installed LIGO Data Grid

%prep
%setup -q

%build

%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}/usr/bin
install --mode=0755 ldg-version ${RPM_BUILD_ROOT}/usr/bin/ldg-version

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(0755,root,root)
/usr/bin/ldg-version

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Wed Mar 09 2016 Adam Mercer <adam.mercer@ligo.org> 1.1.4-2
- change gsissh dependency to path, to support both SL6 and SL7

* Mon Oct 19 2015 Adam Mercer <adam.mercer@ligo.org> 1.1.4-1
- use /usr/bin/python as the interpreter

* Sun Nov 23 2014 Adam Mercer <adam.mercer@ligo.org> 1.1.3-2
- break Requires up into mutliple lines
- add dependency on ldg-upgrade

* Sat Nov 22 2014 Adam Mercer <adam.mercer@ligo.org> 1.1.3-1
- report version of ldg-upgrade

* Thu Aug 8 2013 Adam Mercer <adam.mercer@ligo.org> 1.1.1-1
- report version of ecp-cookie-init
- don't report versions of DOEGrids helper scripts

* Mon Feb 25 2013 Adam Mercer <adam.mercer@ligo.org> 1.0.10-1
- report version of ligo-proxy-init
- report version of curl
- report version of lib{xml,xslt,esxlt}

* Thu Feb 7 2013 Adam Mercer <adam.mercer@ligo.org> 1.0.9-1
- fix version parsing on Debian

* Wed Feb 6 2013 Adam Mercer <adam.mercer@ligo.org> 1.0.8-1
- add support for Red Hat Enterprise variants

* Fri Sep 7 2012 Adam Mercer <adam.mercer@ligo.org> 1.0.7-1
- display location of openssl client
- display location of python interpreter

* Wed Apr 25 2012 Adam Mercer <adam.mercer@ligo.org> 1.0.6-1
- report versions of ldg-version and ldg-upgrade (if appropriate)
- add --openssl option to report version of OpenSSL client
- add --python option to report version of Python
- add --all option to display all version info

* Thu Mar 8 2012 Adam Mercer <adam.mercer@ligo.org> 1.0.5-1
- report version correctly for release and devel gsi patches

* Thu Jan 26 2012 Adam Mercer <adam.mercer@ligo.org> 1.0.4-1
- report Mac OS X build number as well as version

* Wed Nov 9 2011 Adam Mercer <adam.mercer@ligo.org> 1.0.3-1
- fix squeeze dependencies
- remove unused globus_location variable for linux

* Wed Nov 2 2011 Adam Mercer <adam.mercer@ligo.org> 1.0.2-1
- add Scientific Linux support

* Wed Nov 2 2011 Adam Mercer <adam.mercer@ligo.org> 1.0.1-2
- fix dependencies for Scientific Linux 6

* Tue May 31 2011 Adam Mercer <adam.mercer@ligo.org> 1.0.1-1
- use format specifier instead of string concatenation
- more robust method for parsing version
- consistent error messages

* Fri Apr 26 2011 Adam Mercer <adam.mercer@ligo.org> 1.0.0-1
- initial version
