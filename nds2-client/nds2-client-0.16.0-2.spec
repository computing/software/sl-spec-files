#========================================================================
%define name    nds2-client
%define version 0.16.0
%define release 2
%define _prefix /usr
%define _includedir %{_prefix}/include/%{name}
%define _pkgdocdir  %{_defaultdocdir}/%{name}

%define _use_internal_dependency_generator 0
%define __find_requires %{_builddir}/%{name}-%{version}/config/nds-find-requires

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Sanity checks
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#========================================================================
# Main spec file
#========================================================================
Name:           %{name}
Summary:        NDS2 Client interface
Version:        %{version}
Release:        %{release}%{?dist}
License:        GPL
Group:          LSC Software/Data Analysis
Source:         http://software.ligo.org/lscsoft/source/%{name}-%{version}.tar.gz
Packager:       Edward Maros (ed.maroso@ligo.org)
URL:            https://wiki.ligo.org/DASWG/NDSClient
BuildRoot:      %{buildroot}
BuildRequires:  gcc, gcc-c++, glibc
BuildRequires:  libstdc++-static
%if 0%{?rhl} <= 7 || 0%{?sl7} <= 7
BuildRequires:  cmake3 >= 3.6
BuildRequires:  cmake >= 2.6
%else
BuildRequires:  cmake >= 3.6
%endif
BuildRequires:  make
BuildRequires:  doxygen, graphviz
BuildRequires:  rpm-build
BuildRequires:  gawk, pkgconfig
BuildRequires:  cyrus-sasl-devel, cyrus-sasl-gssapi
BuildRequires:  libcurl-devel
Requires:       cyrus-sasl, cyrus-sasl-gssapi
Provides:       %{name} = %{version}
Obsoletes:      %name < %version
Prefix:         %_prefix

%description
The NDS2 client interface allow the user to down-load LIGO data from V1 and V2 LIGO Network Data Servers.

%package devel
Group: Development/Scientific
Summary: NDS2 development headers and documentation
Requires: %{name}-headers = %{version}, %{name} = %{version}, cyrus-sasl-devel
%description devel
This package supports development using the nds2 client protocol.

%package man
Group: Development/Scientific
Summary: NDS2 client man pages files
Requires: %{name} = %{version}
BuildArch: noarch
%description man
This package contains the man pages for the nds2-client package and its
high-level language extensions.

%package doc
Group: Development/Scientific
Summary: NDS2 client documentation files
Requires: %{name} = %{version}
BuildArch: noarch
%description doc
This package contains the doxygen formated pages for the nds2-client package and its
high-level language extensions.

%package headers
Group: Development/Scientific
Summary: NDS2 headers
BuildArch: noarch
%description headers
This provides a separate package to install the headers shared by the 64 and 32 bit versions.

%package all
Group:     Development/Scientific
Summary:   NDS2 Client interface
BuildArch: noarch
Requires:  %{name} = %{version}
Requires:  %{name}-devel = %{version}
Requires:  %{name}-man = %{version}
Requires:  %{name}-doc = %{version}
Requires:  %{name}-headers = %{version}
%description all
 The Network Data Server (NDS) is a TCP/IP protocol for retrieving
 online or archived data from thousands of instrument channels at LIGO
 (Laser Interferometer Gravitational-Wave Observatory) sites and data
 analysis clusters.  Version 2 of the protocol includes communication
 with Kerberos and GSSAPI.
 .
 This package installs all NDS2 client packages, including libraries,
 language bindings, binary interface, and documentation.

#----------------------------------------------
# Get onto the fun of building the NDS software
#----------------------------------------------

%prep
%setup -q

%build
%if %{?cmake3:1}%{!?cmake3:0}
%cmake3 -DWITH_SASL=yes -DWITH_GSSAPI=no -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 .
%define ctest ctest3
%else
%cmake -DWITH_SASL=yes -DWITH_GSSAPI=no -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 .
%define ctest ctest
%endif

%install
make install DESTDIR=%{buildroot}

%check
%ctest -V %{?_smp_mflags}

#----------------------------------------------
# Remove files that will not be packaged based
# on the arch type
#----------------------------------------------

#----------------------------------------------
# Do the noarch files
#----------------------------------------------
%files headers
%{_includedir}/*.h
%{_includedir}/*.hh

%files man
%{_mandir}

%files doc
%{_docdir}

#----------------------------------------------
# Handle binary packages
#----------------------------------------------

%files
%defattr(-,root,root)
%_bindir/nds-client-config
%_bindir/nds2-tunnel
%_bindir/nds2_channel_source
%_bindir/nds_query
%_libdir/libndsclient*.so.*
%_libdir/libndscxx*.so.*
%_sysconfdir/*-user-env*
%_sysconfdir/%{name}/%{name}.cfg

%files devel
%_libdir/cmake/nds2-client/*.cmake
%_libdir/libndsclient*.so
%_libdir/libndscxx*.so
%_libdir/libndsclient*.a
%_libdir/libndscxx*.a
%_libdir/nds2-client/ndswrapcxx/*.o
%_libdir/pkgconfig
%_libexecdir/nds2-client/*

#------------------------------------------------------------------------
# All - metapackage
#------------------------------------------------------------------------
%files all
