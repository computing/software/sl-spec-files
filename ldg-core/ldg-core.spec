Name: ldg-core
Version: 5.2.4
Release: 3%{?dist}
Group: LDG
License: BSD / Globus
Summary: LDG Core Packages
Packager: Adam Mercer <adam.mercer@ligo.org>
Requires: globus-gass-copy-progs
Requires: globus-gsi-cert-utils-progs
Requires: globus-proxy-utils
Requires: ligo-ca-certs
Requires: ligo-proxy-utils
Requires: osg-ca-certs >= 1.40
BuildArch: noarch

# cilogon-ca-certs not needed with osg-ca-certs >= 1.40
Obsoletes: cilogon-ca-certs

%description
This meta-package will pull in the core packages required for the LDG.

%install

%files

%changelog
* Fri Nov 21 2014 Adam Mercer <adam.mercer@ligo.org> 5.2.4-3
- don't provide cilogon-ca-certs

* Thu Nov 20 2014 Adam Mercer <adam.mercer@ligo.org> 5.2.4-2
- use multiple Requires lines
- fix changelog order

* Wed Nov 05 2014 Adam Mercer <adam.mercer@ligo.org> 5.2.4-1
- require osg-ca-certs-1.40 or greater
- no longer need cilogon-ca-certs (provided by osg-ca-certs)

* Wed Apr 16 2014 Adam Mercer <adam.mercer@ligo.org> 5.2.3-1
- add dependency on globus-gass-copy-progs

* Sat Feb 22 2014 Adam Mercer <adam.mercer@ligo.org> 5.2.2-1
- add dependency on ligo-proxy-utils

* Wed Feb 20 2013 Adam Mercer <adam.mercer@ligo.org> 5.2.1-1
- add dependencies on cilogon-ca-certs and ligo-ca-certs

* Wed Nov 02 2011 Adam Mercer <adam.mercer@ligo.org> 5.2-1
- update dependencies for SL6

* Wed Apr 27 2011 Adam Mercer <adam.mercer@ligo.org> 5.1-1
- initial version
